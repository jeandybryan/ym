/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {
	/***************************************************************************
	 *                                                                          *
	 * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
	 * etc. depending on your default view engine) your home page.              *
	 *                                                                          *
	 * (Alternatively, remove this and add an `index.html` file in your         *
	 * `assets` directory)                                                      *
	 *                                                                          *
	 ***************************************************************************/

	//  DEFAULT
	'GET  /install': 'IndexController.install',
	'POST  /install': 'IndexController.installApp',
	'POST  /run': 'IndexController.run',
	'POST  /addEvent': 'IndexController.addEvent',

	//  CLIENT APP
	'GET  /': 'IndexController.home',
	'GET  /:lang(es|en)': 'IndexController.home',
	'GET  /:lang(es|en)/therapists': 'IndexController.therapists',
	'GET  /:lang(es|en)/treatments': 'IndexController.treatments',
	'GET  /:lang(es|en)/treatment/:slug': 'IndexController.treatment',
	'GET  /:lang(es|en)/therapist/:username': 'IndexController.therapist',
	'GET  /:lang(es|en)/request/:username/:slug': 'IndexController.request',

	//  ADMIN APP
	'GET  /admin': 'AdminController.home',
	'GET  /admin/media/:id': 'IndexController.findOneMedia',
	'GET  /admin/*': 'AdminController.home',

	// AUHT
	'POST /api/v1/signin': 'AuthController.signin',
	'POST /api/v1/signup': 'AuthController.signup',

	//  USER API REST
	'GET /api/v1/users': 'UserController.find',
	'GET /api/v1/users/:slug': 'UserController.findOne',
	'POST /api/v1/users': 'UserController.create',
	'PUT /api/v1/users': 'UserController.update',
	'PUT /api/v1/users/password': 'UserController.password',
	'PUT /api/v1/users/active': 'UserController.active',
	'DELETE /api/v1/users/:slug': 'UserController.destroy',
	'POST /api/v1/users/photo/:id': 'UserController.uploadPhoto',
	'DELETE /api/v1/users/photo/:id': 'UserController.deletePhoto',
	'PUT /api/v1/users/profile': 'UserController.profile',
	'PUT /api/v1/users/profile/password': 'UserController.profilePassword',

	//  THERAPIST API REST
	'GET /api/v1/therapists': 'TherapistController.find',
	'GET /api/v1/therapists/:slug': 'TherapistController.findOne',
	'POST /api/v1/therapists': 'TherapistController.create',
	'PUT /api/v1/therapists': 'TherapistController.update',
	'PUT /api/v1/therapists/password': 'TherapistController.password',
	'PUT /api/v1/therapists/active': 'TherapistController.active',
	'DELETE /api/v1/therapists/:slug': 'TherapistController.destroy',
	'PUT /api/v1/therapists/events': 'TherapistController.updateEvents',
	'PUT /api/v1/therapists/profile': 'TherapistController.profile',

	//  MASSAGE API REST
	'GET /api/v1/massages': 'MassageController.find',
	'GET /api/v1/massages/:slug': 'MassageController.findOne',
	'POST /api/v1/massages': 'MassageController.create',
	'POST /api/v1/massages/photos/:id': 'MassageController.uploadPhotos',
	'DELETE /api/v1/massages/photos/:id': 'MassageController.deletePhotos',
	'PUT /api/v1/massages': 'MassageController.update',
	'DELETE /api/v1/massages/:slug': 'MassageController.destroy'

	/***************************************************************************
	 *                                                                          *
	 * Custom routes here...                                                    *
	 *                                                                          *
	 * If a request to a URL doesn't match any of the custom routes above, it   *
	 * is matched against Sails route blueprints. See `config/blueprints.js`    *
	 * for configuration options and examples.                                  *
	 *                                                                          *
	 ***************************************************************************/
};
