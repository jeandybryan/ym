/*global Sly */
jQuery(function($) {
	'use strict';

	// ==========================================================================
	//   Header example
	// ==========================================================================
	var $example = $('#example');
	var $frame = $example.find('.frame');
	window.frr = $frame;
	var sly = new Sly($frame, {
		horizontal: 1,
		itemNav: 'forceCentered',
		activateMiddle: 1,
		smart: 1,
		activateOn: 'click',
		mouseDragging: 1,
		touchDragging: 1,
		releaseSwing: 1,
		startAt: daysData.startAt,
		scrollBar: $example.find('.scrollbar'),
		scrollBy: 1,
		pagesBar: $example.find('.pages'),
		activatePageOn: 'click',
		speed: 200,
		moveBy: 600,
		elasticBounds: 1,
		dragHandle: 1,
		dynamicHandle: 1,
		clickBar: 1,

		// Buttons
		forward: $example.find('.forward'),
		backward: $example.find('.backward'),
		prev: $example.find('.prev'),
		next: $example.find('.next'),
		prevPage: $example.find('.prevPage'),
		nextPage: $example.find('.nextPage')
	}).init();

	var sessions = [];
	var time = null;
	var dayData = [];
	var prices = treatment.prices;
	var posPrice = 0;
	var data = {};
	$('#ym-time').append(prices[posPrice].time + ' min');
	$('#next_price_time').click(function() {
		posPrice++;
		if (posPrice >= prices.length) posPrice = 0;
		$('#ym-time').empty();
		$('#ym-time').append(prices[posPrice].time + ' min');
		updateInfo();
	});
	$('#prev_price_time').click(function() {
		posPrice--;
		if (posPrice < 0) posPrice = prices.length - 1;
		$('#ym-time').empty();
		$('#ym-time').append(prices[posPrice].time + ' min');
		updateInfo();
	});

	function initializeVars(pos) {
		dayData = daysData.days[pos];
		sessions = [
			{
				id: 0,
				h: 0,
				m: 0,
				hm: 'am',
				text: sessionsLang[0]
			},
			{
				id: 1,
				h: 6,
				m: 0,
				hm: 'am',
				text: sessionsLang[1]
			},
			{
				id: 2,
				h: 12,
				m: 0,
				hm: 'pm',
				text: sessionsLang[2]
			},
			{
				id: 3,
				h: 18,
				m: 0,
				hm: 'pm',
				text: sessionsLang[3]
			}
		];

		dayData.events.forEach(function(element) {
			if (element.title == 'Madrugada bloqueada') {
				sessions = sessions.filter(function(elem) {
					return elem.id !== 0;
				});
			}
			if (element.title == 'Mañana bloqueada') {
				sessions = sessions.filter(function(elem) {
					return elem.id !== 1;
				});
			}
			if (element.title == 'Tarde bloqueada') {
				sessions = sessions.filter(function(elem) {
					return elem.id !== 2;
				});
			}
			if (element.title == 'Noche bloqueada') {
				sessions = sessions.filter(function(elem) {
					return elem.id !== 3;
				});
			}

			if (element.title == 'Día bloqueado') {
				sessions = [];
			}
		});
		if (!sessions.length) {
			$('#book_data').fadeOut('slow');
		} else {
			$('#book_data').fadeIn('slow');
			sessions[0].active = true;
			//	Sessions
			$('#book_data_options_sessions').empty();
			$('#book_data_options_sessions').append(sessions[0].text);
			//	Times
			$('#book_data_options_time').empty();
			var h = sessions[0].h;
			if (sessions[0].h < 10) {
				h = '0' + h;
			}
			var m = sessions[0].m;
			if (sessions[0].m < 10) {
				m = '0' + m;
			}
			time = h + ':' + m + ' ' + sessions[0].hm;
			$('#book_data_options_time').append(time);
		}

		updateInfo(dayData);
	}

	function updateInfo() {
		moment.locale(langYm);
		var date = moment(new Date(dayData.y, dayData.m, dayData.d));
		var texts = {
			es: `Usted está reservando  ${prices[posPrice].time} mins de ${
				treatment.name[langYm]
			} con ${therapist.firstname + ' ' + therapist.lastname} en ${date.format('MMMM')}, ${dayData.d}, ${
				dayData.y
			} a las ${time}.`,
			en: `You are booking a ${prices[posPrice].time} mins of ${
				treatment.name[langYm]
			} massage with ${therapist.firstname + ' ' + therapist.lastname} on ${date.format('MMMM')}, ${dayData.d}, ${
				dayData.y
			} at ${time}.`
		};
		var text = texts[langYm];

		$('#bi-text').empty();
		$('#bi-text').append(text);

		$('.ym-info-price').empty();
		$('.ym-info-price').append('$ ' + prices[posPrice].price.toFixed(2));

		data = {
			price: prices[posPrice].id,
			therapist: therapist.therapist.id,
			treatment: treatment.id,
			time: time,
			date: date.unix(),
			text: ''
		};
		console.log(data);
	}

	initializeVars(daysData.startAt);

	sly.on('change', function(eventName) {
		console.log(eventName); //
		console.log(this.rel.activeItem);
		initializeVars(this.rel.activeItem);
	});

	$('#next_sessions').click(function() {
		for (var i = 0; i < sessions.length; i++) {
			if (sessions[i].active) {
				sessions[i].active = false;
				$('#book_data_options_sessions').empty();
				var pos;
				if (i + 1 >= sessions.length) {
					sessions[0].active = true;
					pos = 0;
					$('#book_data_options_sessions').append(sessions[0].text);
				} else {
					sessions[i + 1].active = true;
					pos = i + 1;
					$('#book_data_options_sessions').append(sessions[i + 1].text);
				}
				$('#book_data_options_time').empty();
				var h = sessions[pos].h;
				if (sessions[pos].h < 10) {
					h = '0' + h;
				}
				var m = sessions[pos].m;
				if (sessions[pos].m < 10) {
					m = '0' + m;
				}
				time = h + ':' + m + ' ' + sessions[pos].hm;
				$('#book_data_options_time').append(time);
				break;
			}
		}
		updateInfo();
	});

	$('#prev_sessions').click(function() {
		for (var i = 0; i < sessions.length; i++) {
			if (sessions[i].active) {
				sessions[i].active = false;
				$('#book_data_options_sessions').empty();
				if (i - 1 < 0) {
					sessions[sessions.length - 1].active = true;
					$('#book_data_options_sessions').append(sessions[sessions.length - 1].text);
				} else {
					sessions[i - 1].active = true;
					$('#book_data_options_sessions').append(sessions[i - 1].text);
				}
				$('#book_data_options_time').empty();
				var h = sessions[0].h;
				if (sessions[0].h < 10) {
					h = '0' + h;
				}
				var m = sessions[0].m;
				if (sessions[0].m < 10) {
					m = '0' + m;
				}
				time = h + ':' + m + ' ' + sessions[0].hm;
				$('#book_data_options_time').append(time);
				break;
			}
		}
		updateInfo();
	});

	$('#next_time').click(function() {
		for (var i = 0; i < sessions.length; i++) {
			if (sessions[i].active) {
				if (sessions[i].m == 0) {
					sessions[i].m = 30;
				} else if (sessions[i].m == 30) {
					sessions[i].m = 0;
					sessions[i].h = sessions[i].h + 1;
					if (sessions[i].h == 6) {
						sessions[i].h = 0;
						sessions[i].hm = 'am';
					}
					if (sessions[i].h == 12) {
						sessions[i].h = 6;
						sessions[i].hm = 'am';
					}
					if (sessions[i].h == 18) {
						sessions[i].h = 12;
						sessions[i].hm = 'pm';
					}
					if (sessions[i].h == 24) {
						sessions[i].h = 18;
						sessions[i].hm = 'pm';
					}
				}
				$('#book_data_options_time').empty();
				var h = sessions[i].h;
				if (sessions[i].h < 10) {
					h = '0' + h;
				}
				var m = sessions[i].m;
				if (sessions[i].m < 10) {
					m = '0' + m;
				}
				time = h + ':' + m + ' ' + sessions[i].hm;
				$('#book_data_options_time').append(time);
				break;
			}
		}
		updateInfo();
	});

	$('#prev_time').click(function() {
		for (var i = 0; i < sessions.length; i++) {
			if (sessions[i].active) {
				if (sessions[i].h == 0 && sessions[i].m == 0) {
					sessions[i].h = 5;
					sessions[i].m = 30;
				} else if (sessions[i].h == 6 && sessions[i].m == 0) {
					sessions[i].h = 11;
					sessions[i].m = 30;
				} else if (sessions[i].h == 12 && sessions[i].m == 0) {
					sessions[i].h = 17;
					sessions[i].m = 30;
				} else if (sessions[i].h == 18 && sessions[i].m == 0) {
					sessions[i].h = 23;
					sessions[i].m = 30;
				} else if (sessions[i].m == 0) {
					sessions[i].m = 30;
					sessions[i].h--;
				} else {
					sessions[i].m = 0;
				}
				$('#book_data_options_time').empty();
				var h = sessions[i].h;
				if (sessions[i].h < 10) {
					h = '0' + h;
				}
				var m = sessions[i].m;
				if (sessions[i].m < 10) {
					m = '0' + m;
				}
				time = h + ':' + m + ' ' + sessions[i].hm;
				$('#book_data_options_time').append(time);
				break;
			}
		}
		updateInfo();
	});

	$('#book-now').click(function() {
		data.text = $('#kitext').val();
		$('.booking-info-parent').fadeOut(function() {
			$('.request-process').fadeIn();
			$.post('/complete', data)
				.done(function(data) {
					console.log(data);
					$('.request-process').fadeOut(function() {
						$('.request-process-success').fadeIn();
					});
				})
				.fail(function(error) {
					console.log(error);
					$('.request-process').fadeOut(function() {
						$('.request-process-error').fadeIn();
					});
				});
		});
	});
});
