var pathBase = '/js/admin/app';

(function() {
	'use strict';
	angular.module('YumaMassage').config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider, $locationProvider, $urlRouterProvider, stateHelperProvider) {
		$locationProvider.html5Mode(true);
		$urlRouterProvider.otherwise('/admin');

		$stateProvider
			.state('auth', {
				templateUrl: pathBase + '/auth/auth.html'
			})
			.state('auth.signin', {
				url: '/admin/signin',
				templateUrl: pathBase + '/auth/signin.html'
			})
			.state('auth.signup', {
				url: '/admin/signup',
				templateUrl: pathBase + '/auth/signup.html'
			})
			.state('admin', {
				templateUrl: pathBase + '/dashboard/dashboard.html'
			})
			.state('admin.home', {
				url: '/admin',
				templateUrl: pathBase + '/dashboard/home/home.html'
			})
			.state('admin.users', {
				url: '/admin/users',
				templateUrl: pathBase + '/dashboard/users/users.html'
			})
			.state('admin.therapists', {
				url: '/admin/therapists',
				templateUrl: pathBase + '/dashboard/therapists/therapists.html'
			})
			.state('admin.massages', {
				url: '/admin/massages',
				templateUrl: pathBase + '/dashboard/massages/massages.html'
			})
			.state('admin.profile', {
				url: '/admin/profile',
				templateUrl: pathBase + '/dashboard/profile/profile.html'
			})
			.state('admin.403', {
				url: '/admin/403',
				templateUrl: pathBase + '/dashboard/pages/403.html'
			});
	}
})();
