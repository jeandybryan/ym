(function () {
  "use strict";
  angular.module("YumaMassage").config(config);

  /** @ngInject */
  function config($httpProvider, httpConfig) {
    httpConfig($httpProvider);
  }
})();
