(function() {
	'use strict';
	angular.module('YumaMassage').run(runBlock);

	/** @ngInject */
	function runBlock($rootScope, ngValidateFactory, $localStorage, $http, $location, jwtHelper) {
		$rootScope.page = 'home';

		ngValidateFactory.strategies.emailPattern = [
			{
				value: ngValidateFactory.required,
				message: 'Este campo es requerido'
			},
			{
				value: ngValidateFactory.emailPattern,
				message: 'El email no es válido'
			}
		];

		ngValidateFactory.strategies.customRequired = [
			{
				value: ngValidateFactory.required,
				message: 'Este campo es requerido'
			}
		];

		toastr.options.closeButton = false;
		toastr.options.progressBar = false;
		toastr.options.debug = false;
		toastr.options.positionClass = 'toast-top-right';
		toastr.options.showDuration = 333;
		toastr.options.hideDuration = 333;
		toastr.options.timeOut = 4000;
		toastr.options.extendedTimeOut = 4000;
		toastr.options.showEasing = 'swing';
		toastr.options.hideEasing = 'swing';
		toastr.options.showMethod = 'slideDown';
		toastr.options.hideMethod = 'slideUp';

		// type = ["", "info", "success", "warning", "danger"];
		$rootScope.showNotification = function(color, message, positionClass = 'toast-top-right') {
			toastr.options.positionClass = positionClass;
			switch (color) {
				case 'success':
					toastr.success(message, '');
					break;
				case 'warning':
					toastr.warning(message, '');
					break;
				case 'danger':
					toastr.error(message, '');
					break;
				default:
					toastr.info(message, '');
			}
		};

		$rootScope.handleErrorResponse = function(res) {
			if ((res.data.name && res.data.name == 'TokenExpiredError') || res.data.code == 'E_USER_NOT_FOUND') {
				$rootScope.logout();
			}
			let message = 'Lo sentimos, vuelva ha intentarlo más tarde';
			let type = 'danger';

			if (res.data.code == 'E_VALIDATION') {
				if (res.data.invalidAttributes.email) {
					angular.forEach(res.data.invalidAttributes.email, function(value) {
						if (value.rule == 'unique') {
							message = 'El EMAIL ya está en uso';
							type = 'warning';
						}
					});
				}
				if (res.data.invalidAttributes.username) {
					angular.forEach(res.data.invalidAttributes.username, function(value) {
						if (value.rule == 'unique') {
							message = 'El USUARIO ya está en uso';
							type = 'warning';
						}
					});
				}
			}

			$rootScope.showNotification(type, message, 'toast-bottom-center');
		};

		$rootScope.logout = function() {
			// remove user from local storage and clear http auth header
			delete $localStorage.token;
			$http.defaults.headers.common.Authorization = '';
			window.location = '/admin/signin';
		};

		if ($localStorage.token) {
			var tokenPayload = jwtHelper.decodeToken($localStorage.token);
			$rootScope.currentUser = tokenPayload.user;
			console.log($rootScope.currentUser);
			$http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;
		}

		// redirect to login page if not logged in and trying to access a restricted page
		$rootScope.$on('$locationChangeStart', function(event, next, current) {
			var publicPages = ['/admin/signin', '/admin/signup'];
			var adminPages = ['/admin/users', '/admin/therapists', '/admin/massages'];

			var restrictedPage = publicPages.indexOf($location.path()) === -1;
			if (restrictedPage && !$localStorage.token) {
				$location.path('/admin/signin');
			}

			if (!$rootScope.currentUser.isAdmin) {
				var restrictedPage = adminPages.indexOf($location.path()) >= 0;
				if (restrictedPage || !$localStorage.token) {
					$location.path('/admin/signin');
				}
			}
		});

		$rootScope.menubarVisible = true;
		Dropzone.autoDiscover = false;
		$rootScope.toggle = function() {
			$rootScope.menubarVisible = !$rootScope.menubarVisible;
		};
	}
})();
