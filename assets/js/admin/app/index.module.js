(function() {
	'use strict';
	/**
   * Main module of the YumaMassage
   */
	angular.module('YumaMassage', [
		'ui.router',
		'ui.router.stateHelper',
		'ngResource',
		'ngValidateModule',
		'angular-jwt',
		'ngStorage',
		'ui.calendar',
		'ui.bootstrap',
		'thatisuday.dropzone',
		'ui.select2'
	]);
})();
