(function() {
	'use strict';

	angular.module('YumaMassage').factory('api', apiService);

	/** @ngInject */
	function apiService($resource) {
		var api = {};

		api.baseApi = '/api/v1/';

		api.headers = {
			'Content-Type': 'application/json',
			charset: 'UTF-8'
		};

		api.headers = {
			token: '@token'
		};

		api.users = $resource(
			api.baseApi + 'users/:id',
			{ id: '@id' },
			{
				get: { method: 'GET', cache: false },
				post: { method: 'POST', cache: false },
				put: { method: 'PUT', cache: false },
				delete: { method: 'DELETE', cache: false }
			}
		);

		api.users.password = $resource(
			api.baseApi + 'users/password',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.users.active = $resource(
			api.baseApi + 'users/active',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.users.photo = $resource(
			api.baseApi + 'users/photo/:id',
			{ id: '@id' },
			{
				delete: {
					method: 'DELETE',
					cache: false
				}
			}
		);

		api.users.profile = $resource(
			api.baseApi + 'users/profile',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);
		api.users.profilePassword = $resource(
			api.baseApi + 'users/profile/password',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.therapists = $resource(
			api.baseApi + 'therapists/:id',
			{ id: '@id' },
			{
				get: { method: 'GET', cache: false },
				post: { method: 'POST', cache: false },
				put: { method: 'PUT', cache: false },
				delete: { method: 'DELETE', cache: false }
			}
		);

		api.therapists.password = $resource(
			api.baseApi + 'therapists/password',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.therapists.events = $resource(
			api.baseApi + 'therapists/events',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.therapists.active = $resource(
			api.baseApi + 'therapists/active',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.therapists.profile = $resource(
			api.baseApi + 'therapists/profile',
			{},
			{
				put: {
					method: 'PUT',
					cache: false
				}
			}
		);

		api.signin = $resource(
			api.baseApi + 'signin',
			{},
			{
				post: {
					method: 'POST',
					cache: false
				}
			}
		);

		api.signup = $resource(
			api.baseApi + 'signup',
			{},
			{
				post: {
					method: 'POST',
					cache: false
				}
			}
		);

		api.massages = $resource(
			api.baseApi + 'massages/:id',
			{ id: '@id' },
			{
				get: { method: 'GET', cache: false },
				post: { method: 'POST', cache: false },
				put: { method: 'PUT', cache: false },
				delete: { method: 'DELETE', cache: false }
			}
		);

		api.massages.photos = $resource(
			api.baseApi + 'massages/photos/:id',
			{ id: '@id' },
			{
				delete: {
					method: 'DELETE',
					cache: false
				}
			}
		);

		return api;
	}
})();
