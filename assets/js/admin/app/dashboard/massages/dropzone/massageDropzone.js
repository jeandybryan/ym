(function() {
	'use strict';

	// Usage:
	//
	// Creates:
	//

	angular.module('YumaMassage').component('massageDropzone', {
		templateUrl: '/js/admin/app/dashboard/massages/dropzone/massageDropzone.html',
		controller: massageDropzoneController,
		controllerAs: '$ctrl',
		bindings: {
			id: '=',
			massage: '='
		}
	});

	massageDropzoneController.$inject = ['$rootScope', '$scope', '$localStorage', '$timeout', 'api'];
	function massageDropzoneController(rootScope, scope, localStorage, timeout, api) {
		var $ctrl = this;

		////////////////

		$ctrl.$onInit = function() {
			scope.dzMethods = {};
			scope.myDz = null;

			scope.dzOptions = {
				url: '/api/v1/massages/photos/' + $ctrl.id,
				paramName: 'photos',
				maxFilesize: '10',
				maxFiles: '10',
				acceptedFiles: 'image/jpeg, images/jpg, image/png',
				addRemoveLinks: true,
				headers: { Authorization: 'Bearer ' + localStorage.token },
				dictDefaultMessage: 'Click to add or drop photos (10 max)',
				dictRemoveFile: '',
				dictResponseError: 'Could not upload this photo'
			};

			scope.dzCallbacks = {
				addedfile: function(file) {
					scope.newFile = file;
					if (file.isMock) {
						scope.myDz.createThumbnailFromUrl(file, file.serverImgUrl, null, true);
					}
				},
				removedfile: function(file) {
					api.massages.photos
						.delete({
							id: $ctrl.massage,
							pos: file.pos
						})
						.$promise.then(function(response) {
							if (response.massage) {
								rootScope.showNotification(
									'success',
									'La acción ha sido satisfactoria!!',
									'toast-bottom-center'
								);
							} else {
								rootScope.showNotification(
									'danger',
									'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
									'toast-bottom-center'
								);
							}
						})
						.catch(function(response) {
							rootScope.handleErrorResponse(response);
						});
				},
				success: function(file, xhr) {
					scope.$emit('action', { name: 'add-media', massage: xhr.massage });
				},
				error: function(file, xhr) {
					var response = {
						data: xhr
					};
					rootScope.handleErrorResponse(response);
				}
			};
		};
		$ctrl.$onChanges = function(changesObj) {};
		$ctrl.$onDestroy = function() {
			console.log("Destroy component");
		};
	}
})();

