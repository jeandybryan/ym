(function() {
	'use strict';

	angular.module('YumaMassage').controller('MassagesController', MassagesController);

	MassagesController.$inject = ['$scope', '$rootScope', 'api', '$filter', '$location'];

	function MassagesController(scope, rootScope, api, filter, $location) {
		var vm = this;
		vm.showView = 'list';
		vm.showTab = 'tab1';
		vm.skip = 0;
		vm.limit = 10;
		vm.currentPage = 1;

		vm.massages = [];
		vm.massage = {
			name: {
				es: '',
				en: ''
			},
			description: {
				es: '',
				en: ''
			},
			indications: [],
			contraindications: [],
			prices: [],
			color: '#000'
		};
		vm.price = {
			price: 0,
			time: 0
		};
		vm.media = null;

		vm.changeView = changeView;
		vm.fetchMassages = fetchMassages;
		vm.add = add;
		vm.destroy = destroy;
		vm.edit = edit;
		vm.save = save;
		vm.alertRemove = alertRemove;
		vm.removeAlertRemove = removeAlertRemove;
		vm.pageChanged = pageChanged;
		vm.doAction = doAction;

		vm.indication = {
			es: '',
			en: ''
		};

		vm.showIndications = showIndications;
		vm.addIndication = addIndication;
		vm.destroyIndication = destroyIndication;

		vm.showContraindications = showContraindications;
		vm.addContraindication = addContraindication;
		vm.destroyContraindication = destroyContraindication;

		vm.showAddPrices = showAddPrices;
		vm.addPrice = addPrice;
		vm.destroyPrice = destroyPrice;

		vm.addMedias = addMedias;
		vm.viewMedias = viewMedias;
		vm.destroyMedia = destroyMedia;

		////////////////

		activate();
		vm.fetchMassages();

		function activate() {
			rootScope.page = 'massages';
			scope.$on('action', function(event, data) {
				if (data.name === 'add-media') {
					vm.massage.medias = angular.copy(data.massage.medias);
				}
			});
		}

		function changeView(view) {
			if (view === 'list') {
				window.location.reload(true);
			} else {
				vm.showView = view;
				if (view === 'add') {
					vm.showView = view;
					vm.massage = {
						name: {
							es: '',
							en: ''
						},
						description: {
							es: '',
							en: ''
						},
						indications: [],
						contraindications: [],
						prices: [],
						color: '#000'
					};
				}
			}
		}

		function fetchMassages() {
			api.massages
				.get({
					skip: vm.skip,
					limit: vm.limit
				})
				.$promise.then(function(response) {
					vm.massages = angular.copy(response.massages);
					vm.totalItems = response.count || 0;
					if (vm.currentPage > 1 && !vm.massages.length) {
						vm.currentPage--;
						vm.skip = vm.skip - vm.limit;
						vm.fetchMassages();
					}
					setTimeout(function() {
						$('.ym-tooltip').tooltip();
					}, 100);
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function add() {
			scope.$broadcast('ng-validate');
			if (scope.addMassage.$valid) {
				var btn = $('#btn-add-loading-state');
				btn.button('loading');
				api.massages
					.post({
						name: vm.massage.name,
						description: vm.massage.description,
						indications: vm.massage.indications,
						contraindications: vm.massage.contraindications,
						color: vm.massage.color
					})
					.$promise.then(function(response) {
						if (response.massage) {
							vm.massages.push(response.massage);
							vm.edit(response.massage);
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function save() {
			scope.$broadcast('ng-validate');
			if (scope.addMassage.$valid) {
				var btn = $('#btn-save-loading-state');
				btn.button('loading');
				api.massages
					.put({
						_id: vm.massage.id,
						name: vm.massage.name,
						description: vm.massage.description,
						indications: vm.massage.indications,
						contraindications: vm.massage.contraindications,
						prices: vm.massage.prices,
						color: vm.massage.color
					})
					.$promise.then(function(response) {
						if (response.massage) {
							vm.massage = angular.copy(response.massage);
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function doAction() {
			if (vm.showView == 'add') vm.add();
			if (vm.showView == 'edit') vm.save();
		}

		function destroy() {
			api.massages
				.delete({
					id: vm.massage.id
				})
				.$promise.then(function(response) {
					if (response.massage) {
						rootScope.showNotification(
							'success',
							'La acción ha sido satisfactoria!!',
							'toast-bottom-center'
						);
						vm.fetchMassages();
						vm.removeAlertRemove();
					} else {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		}

		function edit(massage) {
			vm.massage = angular.copy(massage);
			vm.massage.indications = vm.massage.indications || [];
			vm.massage.contraindications = vm.massage.contraindications || [];
			vm.massage.prices = vm.massage.prices || [];

			$('#cp1').colorpicker('setValue', vm.massage.color);

			vm.changeView('edit');
			setTimeout(function() {
				$('.ym-tooltip').tooltip();
			}, 100);
		}

		function alertRemove(massage) {
			vm.massage = angular.copy(massage);
			angular.element('#removeModal').modal();
		}

		function removeAlertRemove() {
			angular.element('#removeModal').modal('hide');
		}

		function pageChanged() {
			vm.skip = (vm.currentPage - 1) * vm.limit;
			vm.fetchMassages();
		}

		function addMedias() {
			angular.element('#addMediasModal').modal();
		}

		function destroyMedia(media) {
			api.massages.photos
				.delete({
					id: media.id
				})
				.$promise.then(function(response) {
					if (response) {
						var filteredList = filter('filter')(vm.massage.medias, function(m) {
							return m.id !== media.id;
						});
						vm.massage.medias = angular.copy(filteredList);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function viewMedias(media) {
			vm.media = media;
			angular.element('#viewMediasModal').modal();
		}

		//Indications
		function showIndications() {
			angular.element('#indicationsModal').modal();
		}

		function addIndication() {
			vm.massage.indications.push(vm.indication);
			vm.indication = {
				es: '',
				en: ''
			};
			angular.element('#indicationsModal').modal('hide');
		}

		function destroyIndication(pos) {
			vm.massage.indications.splice(pos, 1);
		}

		//Contraindications
		function showContraindications() {
			angular.element('#contraindicationsModal').modal();
		}

		function addContraindication() {
			vm.massage.contraindications.push(vm.indication);
			vm.indication = {
				es: '',
				en: ''
			};
			angular.element('#contraindicationsModal').modal('hide');
		}

		function destroyContraindication(pos) {
			vm.massage.contraindications.splice(pos, 1);
		}

		function showAddPrices() {
			angular.element('#pricesModal').modal();
		}

		function addPrice() {
			vm.price.id = 'new';
			vm.massage.prices.push(vm.price);
			vm.price = {
				price: 0,
				time: 0
			};
			angular.element('#pricesModal').modal('hide');
		}

		function destroyPrice(pos) {
			vm.massage.prices.splice(pos, 1);
		}

		$('#cp1')
			.colorpicker()
			.on('changeColor', function(ev) {
				vm.massage.color = ev.color.toHex();
			});
	}
})();
