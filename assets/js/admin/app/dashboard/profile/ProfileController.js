(function() {
	'use strict';

	angular.module('YumaMassage').controller('ProfileController', ProfileController);

	ProfileController.$inject = ['$rootScope', '$scope', 'api', 'jwtHelper', '$localStorage', '$http', '$filter'];

	function ProfileController(rootScope, scope, api, jwtHelper, localStorage, http, filter) {
		var vm = this;
		activate();
		vm.showView = 'edit';
		vm.showTab = 'tab1';
		vm.mskip = 0;
		vm.mlimit = 10;
		vm.mcurrentPage = 1;
		vm.massages = [];
		var tags = [
			{
				id: 'Es',
				text: 'Español (Es)'
			},
			{
				id: 'En',
				text: 'Inglés (En)'
			},
			{
				id: 'Fr',
				text: 'Francés (Fr)'
			},
			{
				id: 'De',
				text: 'Alemán (De)'
			},
			{
				id: 'It',
				text: 'Italiano (It)'
			},
			{
				id: 'Ru',
				text: 'Ruso (Ru)'
			},
			{
				id: 'Pt',
				text: 'Portugués (Pt)'
			}
		];
		vm.select2Options = {
			multiple: true,
			simple_tags: true,
			tags: tags // Can be empty list.
		};

		vm.save = save;
		vm.changePass = changePass;
		vm.savet = savet;
		vm.showMassages = showMassages;
		vm.fetchMassages = fetchMassages;
		vm.addMassages = addMassages;
		vm.destroyMassage = destroyMassage;

		////////////////

		function activate() {
			rootScope.page = 'profile';
		}

		function save() {
			scope.$broadcast('ng-validate');
			if (scope.editUser.$valid) {
				var btn = $('#btn-save-loading-state');
				btn.button('loading');
				api.users.profile
					.put({
						firstname: rootScope.currentUser.firstname,
						lastname: rootScope.currentUser.lastname,
						email: rootScope.currentUser.email,
						phone: rootScope.currentUser.phone,
						gender: rootScope.currentUser.gender
					})
					.$promise.then(function(response) {
						if (response.token) {
							var tokenPayload = jwtHelper.decodeToken(response.token);
							// store username and token in local storage to keep user logged in between page refreshes
							localStorage.token = response.token;
							rootScope.currentUser = tokenPayload.user;
							// add jwt token to auth header for all requests made by the $http service
							http.defaults.headers.common.Authorization = 'Bearer ' + response.token;
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function changePass() {
			angular.element('#rr_password').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			scope.$broadcast('ng-validate');
			if (rootScope.currentUser.password !== rootScope.currentUser.r_password) {
				angular.element('#rr_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#rr_password').append(element);
			} else if (scope.passUser.$valid && rootScope.currentUser.password === rootScope.currentUser.r_password) {
				var btn = $('#btn-pass-loading-state');
				btn.button('loading');
				api.users.profilePassword
					.put({
						old_password: rootScope.currentUser.old_password,
						password: rootScope.currentUser.password
					})
					.$promise.then(function(response) {
						if (response.user) {
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
						btn.button('reset');
					});
			}
		}

		function savet() {
			angular.element('#yme_languages').removeClass('has-error');
			scope.$broadcast('ng-validate');
			if (!rootScope.currentUser.therapist.languages.length) {
				angular.element('#yme_languages').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Debe seleccionar un idioma</span>`;
				angular.element('#yme_languages').append(element);
			}
			if (scope.therapistForm.$valid && rootScope.currentUser.therapist.languages.length) {
				var btn = $('#btn-savet-loading-state');
				btn.button('loading');
				api.therapists.profile
					.put({
						firstname: rootScope.currentUser.firstname,
						lastname: rootScope.currentUser.lastname,
						email: rootScope.currentUser.email,
						phone: rootScope.currentUser.phone,
						gender: rootScope.currentUser.gender,
						therapist: rootScope.currentUser.therapist
					})
					.$promise.then(function(response) {
						if (response.token) {
							var tokenPayload = jwtHelper.decodeToken(response.token);
							// store username and token in local storage to keep user logged in between page refreshes
							localStorage.token = response.token;
							rootScope.currentUser = tokenPayload.user;
							// add jwt token to auth header for all requests made by the $http service
							http.defaults.headers.common.Authorization = 'Bearer ' + response.token;
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function fetchMassages() {
			api.massages
				.get({
					skip: vm.mskip,
					limit: vm.mlimit,
					massages: JSON.stringify(rootScope.currentUser.therapist.treatments)
				})
				.$promise.then(function(response) {
					vm.massages = angular.copy(response.massages);
					vm.mtotalItems = response.count || 0;
					if (vm.mcurrentPage > 1 && !vm.benefices.length) {
						vm.mcurrentPage--;
						vm.mskip = vm.mskip - vm.mlimit;
						vm.fetchMassages();
					}
					setTimeout(function() {
						$('.ym-tooltip').tooltip();
					}, 100);
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function showMassages() {
			vm.fetchMassages();
			angular.element('#massagesModal').modal();
		}

		function addMassages(massage) {
			rootScope.currentUser.therapist.treatments.push(massage);
			angular.element('#massagesModal').modal('hide');
		}

		function destroyMassage(id) {
			var filteredList = filter('filter')(rootScope.currentUser.therapist.treatments, function(treatment) {
				return treatment.id !== id;
			});
			rootScope.currentUser.therapist.treatments = angular.copy(filteredList);
		}
	}
})();
