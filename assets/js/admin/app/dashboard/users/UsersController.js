(function() {
	'use strict';

	angular.module('YumaMassage').controller('UsersController', UsersController);

	UsersController.$inject = ['$scope', '$rootScope', 'api', '$filter'];

	function UsersController(scope, rootScope, api, filter) {
		var vm = this;
		vm.showView = 'list';
		vm.showTab = 'tab1';
		vm.skip = 0;
		vm.limit = 10;
		vm.currentPage = 1;
		vm.users = [];
		vm.user = {
			username: '',
			firstname: '',
			lastname: '',
			email: '',
			phone: '',
			gender: 'male',
			password: '',
			r_password: ''
		};

		vm.changeView = changeView;
		vm.fetchUsers = fetchUsers;
		vm.add = add;
		vm.destroy = destroy;
		vm.edit = edit;
		vm.save = save;
		vm.changePass = changePass;
		vm.active = active;
		vm.alertRemove = alertRemove;
		vm.removeAlertRemove = removeAlertRemove;
		vm.pageChanged = pageChanged;

		////////////////

		activate();
		vm.fetchUsers();

		function activate() {
			rootScope.page = 'users';
		}
		function changeView(view) {
			vm.showView = view;
			if (view == 'add') {
				vm.user = {
					username: '',
					firstname: '',
					lastname: '',
					email: '',
					phone: '',
					gender: 'male',
					password: '',
					r_password: ''
				};
			}
			if (view == 'list') vm.fetchUsers();
		}

		function fetchUsers() {
			api.users
				.get({ skip: vm.skip, limit: vm.limit })
				.$promise.then(function(response) {
					vm.users = angular.copy(response.users);
					vm.totalItems = response.count || 0;
					if (vm.currentPage > 1 && !vm.users.length) {
						vm.currentPage--;
						vm.skip = vm.skip - vm.limit;
						vm.fetchUsers();
					}
					setTimeout(function() {
						$('.ym-tooltip').tooltip();
					}, 100);
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function add() {
			angular.element('#r_password').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			scope.$broadcast('ng-validate');
			if (vm.user.password !== vm.user.r_password) {
				angular.element('#r_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#r_password').append(element);
			}
			if (scope.addUser.$valid && vm.user.password === vm.user.r_password) {
				var btn = $('#btn-add-loading-state');
				btn.button('loading');
				api.users
					.post({
						username: vm.user.username,
						firstname: vm.user.firstname,
						lastname: vm.user.lastname,
						email: vm.user.email,
						phone: vm.user.phone,
						gender: vm.user.gender,
						password: vm.user.password
					})
					.$promise.then(function(response) {
						if (response.user) {
							vm.users.push(response.user);
							vm.user = {
								username: '',
								firstname: '',
								lastname: '',
								email: '',
								phone: '',
								gender: 'male',
								password: '',
								r_password: ''
							};
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function save() {
			scope.$broadcast('ng-validate');
			if (scope.editUser.$valid) {
				var btn = $('#btn-save-loading-state');
				btn.button('loading');
				api.users
					.put({
						_id: vm.user.id,
						firstname: vm.user.firstname,
						lastname: vm.user.lastname,
						email: vm.user.email,
						phone: vm.user.phone,
						gender: vm.user.gender
					})
					.$promise.then(function(response) {
						if (response.user) {
							vm.user = angular.copy(response.user);
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function changePass() {
			angular.element('#rr_password').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			scope.$broadcast('ng-validate');
			if (vm.user.password !== vm.user.r_password) {
				angular.element('#rr_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#rr_password').append(element);
			} else if (scope.passUser.$valid && vm.user.password === vm.user.r_password) {
				var btn = $('#btn-pass-loading-state');
				btn.button('loading');
				api.users.password
					.put({
						_id: vm.user.id,
						old_password: vm.user.old_password,
						password: vm.user.password
					})
					.$promise.then(function(response) {
						if (response.user) {
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function active(user) {
			api.users.active
				.put({
					_id: user.id,
					active: !user.active
				})
				.$promise.then(function(response) {
					if (response.user) {
						rootScope.showNotification(
							'success',
							'La acción ha sido satisfactoria!!',
							'toast-bottom-center'
						);
						vm.fetchUsers();
					} else {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		}

		function destroy() {
			api.users
				.delete({
					id: vm.user.id
				})
				.$promise.then(function(response) {
					if (response.user) {
						rootScope.showNotification(
							'success',
							'La acción ha sido satisfactoria!!',
							'toast-bottom-center'
						);
						vm.fetchUsers();
						vm.removeAlertRemove();
					} else {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		}

		function edit(user) {
			vm.user = angular.copy(user);
			vm.user.old_password = '';
			vm.user.password = '';
			vm.user.r_password = '';
			vm.changeView('edit');
		}

		function alertRemove(user) {
			vm.user = angular.copy(user);
			angular.element('#removeModal').modal();
		}

		function removeAlertRemove() {
			angular.element('#removeModal').modal('hide');
		}

		function pageChanged() {
			vm.skip = (vm.currentPage - 1) * vm.limit;
			vm.fetchUsers();
		}
	}
})();
