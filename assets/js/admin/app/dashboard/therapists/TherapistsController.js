(function() {
	'use strict';

	angular.module('YumaMassage').controller('TherapistsController', TherapistsController);

	TherapistsController.$inject = ['$scope', '$rootScope', 'api', '$filter'];

	function TherapistsController(scope, rootScope, api, filter) {
		var vm = this;
		vm.showView = 'list';
		vm.showTab = 'tab1';
		vm.skip = 0;
		vm.limit = 10;
		vm.currentPage = 1;
		vm.users = [];
		vm.user = {
			username: '',
			firstname: '',
			lastname: '',
			email: '',
			phone: '',
			gender: 'male',
			password: '',
			r_password: '',
			therapist: {
				yxp: 0,
				languages: [],
				treatments: [],
				medias: [],
				week_day: false,
				weekend: false,
				morning: false,
				afternoon: false,
				evening: false,
				daybreak: false,
				description: {
					es: '',
					en: ''
				}
			}
		};
		vm.time = new Date().getTime();

		var tags = [
			{
				id: 'Es',
				text: 'Español (Es)'
			},
			{
				id: 'En',
				text: 'Inglés (En)'
			},
			{
				id: 'Fr',
				text: 'Francés (Fr)'
			},
			{
				id: 'De',
				text: 'Alemán (De)'
			},
			{
				id: 'It',
				text: 'Italiano (It)'
			},
			{
				id: 'Ru',
				text: 'Ruso (Ru)'
			},
			{
				id: 'Pt',
				text: 'Portugués (Pt)'
			}
		];
		vm.select2Options = {
			multiple: true,
			simple_tags: true,
			tags: tags // Can be empty list.
		};

		vm.mskip = 0;
		vm.mlimit = 10;
		vm.mcurrentPage = 1;
		vm.massages = [];

		vm.changeView = changeView;
		vm.fetchUsers = fetchUsers;
		vm.add = add;
		vm.destroy = destroy;
		vm.edit = edit;
		vm.save = save;
		vm.savet = savet;
		vm.changePass = changePass;
		vm.active = active;
		vm.alertRemove = alertRemove;
		vm.removeAlertRemove = removeAlertRemove;
		vm.pageChanged = pageChanged;
		vm.showMassages = showMassages;
		vm.fetchMassages = fetchMassages;
		vm.mpageChanged = mpageChanged;
		vm.addMassages = addMassages;
		vm.destroyMassage = destroyMassage;
		vm.destroyMedia = destroyMedia;
		vm.calendar = calendar;

		////////////////

		activate();
		vm.fetchUsers();

		function activate() {
			rootScope.page = 'therapists';
			scope.$on('action', function(event, data) {
				if (data.name === 'add-media') {
					vm.user = angular.copy(data.user);
					vm.time = new Date().getTime();
				}
			});
		}

		function changeView(view) {
			vm.showView = view;
			if (view === 'add') {
				vm.user = {
					username: '',
					firstname: '',
					lastname: '',
					email: '',
					phone: '',
					gender: 'male',
					password: '',
					r_password: '',
					therapist: {
						yxp: 0,
						languages: [],
						treatments: [],
						medias: [],
						week_day: false,
						weekend: false,
						morning: false,
						afternoon: false,
						evening: false,
						description: {
							es: '',
							en: ''
						}
					}
				};
			}
			if (view === 'list') vm.fetchUsers();
		}

		function fetchUsers() {
			api.therapists
				.get({
					skip: vm.skip,
					limit: vm.limit
				})
				.$promise.then(function(response) {
					vm.users = angular.copy(response.users);
					vm.totalItems = response.count || 0;
					if (vm.currentPage > 1 && !vm.users.length) {
						vm.currentPage--;
						vm.skip = vm.skip - vm.limit;
						vm.fetchUsers();
					}
					setTimeout(function() {
						$('.ym-tooltip').tooltip();
					}, 100);
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function add() {
			angular.element('#r_password').removeClass('has-error');
			angular.element('#ym_languages').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			scope.$broadcast('ng-validate');
			if (vm.user.password !== vm.user.r_password) {
				angular.element('#r_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#r_password').append(element);
			}
			if (!vm.user.therapist.languages.length) {
				angular.element('#ym_languages').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Debe seleccionar un idioma</span>`;
				angular.element('#ym_languages').append(element);
			}
			if (scope.addUser.$valid && vm.user.therapist.languages.length && vm.user.password === vm.user.r_password) {
				var btn = $('#btn-add-loading-state');
				btn.button('loading');
				api.therapists
					.post({
						username: vm.user.username,
						firstname: vm.user.firstname,
						lastname: vm.user.lastname,
						email: vm.user.email,
						phone: vm.user.phone,
						gender: vm.user.gender,
						password: vm.user.password,
						therapist: vm.user.therapist
					})
					.$promise.then(function(response) {
						if (response.user) {
							vm.users.push(response.user);
							vm.user = {
								username: '',
								firstname: '',
								lastname: '',
								email: '',
								phone: '',
								gender: 'male',
								password: '',
								r_password: '',
								therapist: {
									yxp: 0,
									languages: [],
									treatments: [],
									medias: [],
									week_day: false,
									weekend: false,
									morning: false,
									afternoon: false,
									evening: false,
									description: {
										es: '',
										en: ''
									}
								}
							};
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function save() {
			scope.$broadcast('ng-validate');
			if (scope.editUser.$valid) {
				var btn = $('#btn-save-loading-state');
				btn.button('loading');
				api.therapists
					.put({
						_id: vm.user.id,
						firstname: vm.user.firstname,
						lastname: vm.user.lastname,
						email: vm.user.email,
						phone: vm.user.phone,
						gender: vm.user.gender
					})
					.$promise.then(function(response) {
						if (response.user) {
							vm.user = angular.copy(response.user);
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function savet() {
			angular.element('#yme_languages').removeClass('has-error');
			scope.$broadcast('ng-validate');
			if (!vm.user.therapist.languages.length) {
				angular.element('#yme_languages').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Debe seleccionar un idioma</span>`;
				angular.element('#yme_languages').append(element);
			}
			if (scope.therapistForm.$valid && vm.user.therapist.languages.length) {
				var btn = $('#btn-savet-loading-state');
				btn.button('loading');
				api.therapists
					.put({
						_id: vm.user.id,
						firstname: vm.user.firstname,
						lastname: vm.user.lastname,
						email: vm.user.email,
						phone: vm.user.phone,
						gender: vm.user.gender,
						therapist: vm.user.therapist
					})
					.$promise.then(function(response) {
						if (response.user) {
							vm.user = angular.copy(response.user);
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Lo sentimos, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function changePass() {
			angular.element('#rr_password').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			scope.$broadcast('ng-validate');
			if (vm.user.password !== vm.user.r_password) {
				angular.element('#rr_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#rr_password').append(element);
			} else if (scope.passUser.$valid && vm.user.password === vm.user.r_password) {
				var btn = $('#btn-pass-loading-state');
				btn.button('loading');
				api.therapists.password
					.put({
						_id: vm.user.id,
						old_password: vm.user.old_password,
						password: vm.user.password
					})
					.$promise.then(function(response) {
						if (response.user) {
							rootScope.showNotification(
								'success',
								'La acción ha sido satisfactoria!!',
								'toast-bottom-center'
							);
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
						btn.button('reset');
					})
					.catch(function(response) {
						if (response.status === 403) rootScope.logout();
						rootScope.handleErrorResponse(response);
						btn.button('reset');
					});
			}
		}

		function active(user) {
			api.therapists.active
				.put({
					_id: user.id,
					active: !user.active
				})
				.$promise.then(function(response) {
					if (response.user) {
						rootScope.showNotification(
							'success',
							'La acción ha sido satisfactoria!!',
							'toast-bottom-center'
						);
						vm.fetchUsers();
					} else {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		}

		function destroy() {
			api.therapists
				.delete({
					id: vm.user.id
				})
				.$promise.then(function(response) {
					if (response.user) {
						rootScope.showNotification(
							'success',
							'La acción ha sido satisfactoria!!',
							'toast-bottom-center'
						);
						vm.fetchUsers();
						vm.removeAlertRemove();
					} else {
						rootScope.showNotification(
							'danger',
							'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
							'toast-bottom-center'
						);
					}
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		}

		function edit(user) {
			vm.user = angular.copy(user);
			vm.user.old_password = '';
			vm.user.password = '';
			vm.user.r_password = '';
			vm.changeView('edit');
			scope.$broadcast('action', {
				name: 'reload',
				id: vm.user.id
			});
		}

		function calendar(user) {
			vm.user = angular.copy(user);
			vm.changeView('calendar');
			scope.$broadcast('action', {
				name: 'showCalendar'
			});
		}

		function alertRemove(user) {
			vm.user = angular.copy(user);
			angular.element('#removeModal').modal();
		}

		function removeAlertRemove() {
			angular.element('#removeModal').modal('hide');
		}

		function pageChanged() {
			vm.skip = (vm.currentPage - 1) * vm.limit;
			vm.fetchUsers();
		}

		function mpageChanged() {
			vm.mskip = (vm.mcurrentPage - 1) * vm.mlimit;
			vm.fetchMassages();
		}

		function showMassages() {
			vm.fetchMassages();
			angular.element('#massagesModal').modal();
		}

		function fetchMassages() {
			api.massages
				.get({
					skip: vm.mskip,
					limit: vm.mlimit,
					massages: JSON.stringify(vm.user.therapist.treatments)
				})
				.$promise.then(function(response) {
					vm.massages = angular.copy(response.massages);
					vm.mtotalItems = response.count || 0;
					if (vm.mcurrentPage > 1 && !vm.benefices.length) {
						vm.mcurrentPage--;
						vm.mskip = vm.mskip - vm.mlimit;
						vm.fetchMassages();
					}
					setTimeout(function() {
						$('.ym-tooltip').tooltip();
					}, 100);
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}

		function addMassages(massage) {
			vm.user.therapist.treatments.push(massage);
			angular.element('#massagesModal').modal('hide');
		}

		function destroyMassage(id) {
			var filteredList = filter('filter')(vm.user.therapist.treatments, function(treatment) {
				return treatment.id !== id;
			});
			vm.user.therapist.treatments = angular.copy(filteredList);
		}

		function destroyMedia() {
			api.users.photo
				.delete({
					id: vm.user.medias[0].id
				})
				.$promise.then(function(response) {
					if (response) vm.user.medias = [];
				})
				.catch(function(response) {
					if (response.status === 403) rootScope.logout();
					rootScope.handleErrorResponse(response);
				});
		}
	}
})();
