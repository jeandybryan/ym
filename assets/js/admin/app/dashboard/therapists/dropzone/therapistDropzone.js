(function() {
	'use strict';

	// Usage:
	//
	// Creates:
	//

	angular.module('YumaMassage').component('therapistDropzone', {
		templateUrl: '/js/admin/app/dashboard/therapists/dropzone/therapistDropzone.html',
		controller: therapistDropzoneController,
		controllerAs: '$ctrl',
		bindings: {
			id: '=',
			therapist: '='
		}
	});

	therapistDropzoneController.$inject = ['$rootScope', '$scope', '$localStorage', '$timeout', 'api'];
	function therapistDropzoneController(rootScope, scope, localStorage, timeout, api) {
		var $ctrl = this;

		////////////////

		function reload(id) {
			scope.dzMethods = {};
			scope.myDz = null;

			scope.dzOptions = {
				url: '/api/v1/users/photo/' + id,
				paramName: 'photos',
				maxFilesize: '1',
				maxFiles: '10',
				acceptedFiles: 'image/jpeg, images/jpg, image/png',
				addRemoveLinks: true,
				headers: { Authorization: 'Bearer ' + localStorage.token },
				dictDefaultMessage: 'Click para subir una imagen (1 max)',
				dictRemoveFile: '',
				dictResponseError: 'No se ha podido subir la imagen'
			};

			scope.dzCallbacks = {
				addedfile: function(file) {
					scope.newFile = file;
					if (file.isMock) {
						scope.myDz.createThumbnailFromUrl(file, file.serverImgUrl, null, true);
					}
				},
				success: function(file, xhr) {
					// scope.$emit('action', { name: 'add-media', user: xhr.user });
					// scope.dzMethods.removeAllFiles();
          window.location="/admin/therapists"
				},
				error: function(file, xhr) {
					var response = {
						data: xhr
					};
					rootScope.handleErrorResponse(response);
				}
			};
		}

		$ctrl.$onInit = function() {
			reload($ctrl.id);
			scope.$on('action', function(event, data) {
				if (data.name === 'reload') {
					$ctrl.id = angular.copy(data.id);
					reload($ctrl.id);
				}
			});
		};
		$ctrl.$onChanges = function(changesObj) {
			console.log(changesObj);
		};
		$ctrl.$onDestroy = function() {};
	}
})();
