(function() {
	'use strict';

	// Usage:
	//
	// Creates:
	//

	angular.module('YumaMassage').component('calendar', {
		templateUrl: '/js/admin/app/dashboard/therapists/calendar/calendar.html',
		controller: calendarController,
		controllerAs: '$ctrl',
		bindings: {
			user: '='
		}
	});

	calendarController.$inject = ['$rootScope', '$scope', 'api', 'uiCalendarConfig'];
	function calendarController(rootScope, scope, api, uiCalendarConfig) {
		var $ctrl = this;
		$ctrl.events = [];
		$ctrl.eventsDay = [];
		$ctrl.mode = 'month';

		////////////////

		$ctrl.$onInit = function() {
			$ctrl.initializeEvents($ctrl.user.therapist.events || []);
			setTimeout(function() {
				$ctrl.changeView('month', 'ym-calendar');
				$ctrl._displayDate();
			}, 100);
		};
		$ctrl.$onChanges = function(changesObj) {};
		$ctrl.$onDestroy = function() {};
		$ctrl.loadCalendar = function() {
			$ctrl.selectedDate = new Date();
			/* config object */
			$ctrl.uiConfig = {
				calendar: {
					height: 450,
					editable: false,
					header: false,
					dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
					dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
					eventClick: $ctrl.alertOnEventClick,
					eventDrop: $ctrl.alertOnDrop,
					eventResize: $ctrl.alertOnResize,
					eventRender: $ctrl.eventRender,
					events: $ctrl.events,
					dayClick: function(date, elem) {
						$ctrl.dayClick(date, elem);
					}
				}
			};
		};
		$ctrl.initializeEvents = function(events) {
			$ctrl.events = [];
			$ctrl.eventsDay = [];
			events.forEach(function(elem) {
				elem.start = angular.copy(new Date(elem.start));
				if (!elem.allDay) elem.end = angular.copy(new Date(elem.end));
				$ctrl.events.push(elem);
			});
			$ctrl.user.therapist.events = angular.copy(events);

			$ctrl.loadCalendar();
		};
		$ctrl._displayDate = function() {
			angular.element('.selected-day').empty();
			angular.element('.selected-date').empty();
			angular.element('.selected-day').append(moment($ctrl.selectedDate).format('dddd'));
			angular.element('.selected-date').append(moment($ctrl.selectedDate).format('DD MMMM YYYY'));

			var relem1 = $('.fc-day.fc-widget-content.selected').removeClass('selected');
			var relem2 = $('.fc-day-top.selected').removeClass('selected');

			var elems = $(
				'[data-date=' +
					moment($ctrl.selectedDate).format('YYYY') +
					'-' +
					moment($ctrl.selectedDate).format('MM') +
					'-' +
					moment($ctrl.selectedDate).format('DD') +
					']'
			);
			if (elems) elems.addClass('selected');
		};

		/* alert on eventClick */
		$ctrl.alertOnEventClick = function(date, jsEvent, view) {
			$ctrl.alertMessage = date.title + ' was clicked ';
		};

		/* add and removes an event source of choice */
		$ctrl.addRemoveEventSource = function(sources, source) {
			var canAdd = 0;
			angular.forEach(sources, function(value, key) {
				if (sources[key] === source) {
					sources.splice(key, 1);
					canAdd = 1;
				}
			});
			if (canAdd === 0) {
				sources.push(source);
			}
		};

		/* add custom event*/
		$ctrl.addEvent = function() {
			$ctrl.events.push({
				title: 'Open Sesame',
				start: new Date(y, m, 28),
				end: new Date(y, m, 29),
				className: ['openSesame']
			});
		};

		/* remove event */
		$ctrl.remove = function(index) {
			$ctrl.events.splice(index, 1);
		};

		$ctrl.changeView = function(view, calendar) {
			$ctrl.mode = view;
			uiCalendarConfig.calendars[calendar].fullCalendar('changeView', view);
		};

		$ctrl.changePageCalendar = function(direction, calendar) {
			uiCalendarConfig.calendars[calendar].fullCalendar(direction);
			$ctrl.selectedDate = new Date(uiCalendarConfig.calendars[calendar].fullCalendar('getDate'));
			$ctrl._displayDate();
		};

		$ctrl.goToday = function(calendar) {
			uiCalendarConfig.calendars[calendar].fullCalendar('today');
			$ctrl.selectedDate = new Date(uiCalendarConfig.calendars[calendar].fullCalendar('getDate'));
			$ctrl._displayDate();
		};

		$ctrl.dayClick = function(date, elem) {
			if ($ctrl.selectedDate !== date) {
				$ctrl.selectedDate = date;
				$ctrl.eventsDay = [];
			}
			var fevents = $ctrl.user.therapist.events.filter(
				event =>
					event.start.getFullYear() === parseInt(moment($ctrl.selectedDate).format('YYYY')) &&
					event.start.getMonth() === parseInt(moment($ctrl.selectedDate).format('M') - 1) &&
					event.start.getDate() === parseInt(moment($ctrl.selectedDate).format('DD'))
			);
			$ctrl.eventsDay = $ctrl.eventsDay.concat(fevents);

			$ctrl._displayDate();

			angular.element('#dayModalTitle').empty();
			angular.element('#dayModalTitle').append(moment($ctrl.selectedDate).format('DD MMMM YYYY'));
			angular.element('#dayModal').modal('show');
		};

		$ctrl.renderCalender = function(calendar) {
			if (uiCalendarConfig.calendars[calendar]) {
				uiCalendarConfig.calendars[calendar].fullCalendar('render');
			}
		};

		$ctrl.eventRender = function(event, element, view) {
			element.attr({
				tooltip: event.title,
				'tooltip-append-to-body': true
			});
		};

		$ctrl.addBlockEvent = function(type) {
			var event = {
				type: 'block',
				title: '',
				start: null,
				end: null,
				allDay: false,
				color: '#f00',
				textColor: 'yellow'
			};

			var d = moment($ctrl.selectedDate).format('DD');
			var m = moment($ctrl.selectedDate).format('M') - 1;
			var y = moment($ctrl.selectedDate).format('YYYY');

			if (type == 'daybreak') {
				event.title = 'Madrugada bloqueada';
				event.start = new Date(y, m, d, 0, 0);
				event.end = new Date(y, m, d, 6, 0);
				event.allDay = false;
			}
			if (type == 'morning') {
				event.title = 'Mañana bloqueada';
				event.start = new Date(y, m, d, 6, 0);
				event.end = new Date(y, m, d, 12, 0);
				event.allDay = false;
			}
			if (type == 'afternoon') {
				event.title = 'Tarde bloqueada';
				event.start = new Date(y, m, d, 12, 0);
				event.end = new Date(y, m, d, 18, 0);
				event.allDay = false;
			}
			if (type == 'evening') {
				event.title = 'Noche bloqueada';
				event.start = new Date(y, m, d, 18, 0);
				event.end = new Date(y, m, d, 24, 0);
				event.allDay = false;
			}
			if (type == 'allDay') {
				event.title = 'Día bloqueado';
				event.start = new Date(y, m, d);
				event.allDay = true;
			}
			$ctrl.eventsDay.push(event);
			$ctrl.sortEvents($ctrl.eventsDay);
		};

		$ctrl.removeBlockEvent = function(pos) {
			$ctrl.disableButton = false;
			$ctrl.eventsDay.splice(pos, 1);
		};
		$ctrl.sortEvents = function(events) {
			$ctrl.eventsDay = $ctrl.eventsDay.sort((a, b) => {
				return a.start - b.start;
			});
		};

		$ctrl.saveEventsDay = function() {
			var btn = $('#btn-mloading');
			btn.button('loading');
			var events = [];
			$ctrl.eventsDay.forEach(function(elem) {
				var aux = angular.copy(elem);
				aux.start = {
					y: aux.start.getFullYear(),
					m: aux.start.getMonth(),
					d: aux.start.getDate(),
					h: aux.start.getHours(),
					mi: aux.start.getMinutes()
				};
				if (aux.end) {
					aux.end = {
						y: aux.end.getFullYear(),
						m: aux.end.getMonth(),
						d: aux.end.getDate(),
						h: aux.end.getHours(),
						mi: aux.end.getMinutes()
					};
				}
				delete aux.$$hashKey;
				events.push(aux);
			});
			api.therapists.events
				.put({
					id: $ctrl.user.therapist.id,
					day: {
						d: moment($ctrl.selectedDate).format('DD'),
						m: moment($ctrl.selectedDate).format('M') - 1,
						y: moment($ctrl.selectedDate).format('YYYY')
					},
					events: JSON.stringify(events)
				})
				.$promise.then(function(response) {
					$ctrl.initializeEvents(response.therapist.events);
					btn.button('reset');
					angular.element('#dayModal').modal('hide');
				})
				.catch(function(response) {
					rootScope.handleErrorResponse(response);
					btn.button('reset');
				});
		};

		$ctrl.disableButton = false;
		$ctrl.canShow = function(title) {
			var bd = false;
			angular.forEach($ctrl.eventsDay, function(event) {
				if (event.title === 'Día bloqueado') bd = true;
			});
			if (bd && title !== 'Día bloqueado') {
				$ctrl.disableButton = true;
				return true;
			}
			if (!bd && title === 'Día bloqueado' && $ctrl.eventsDay.length) return true;
			var exist = false;
			angular.forEach($ctrl.eventsDay, function(event) {
				if (event.title === title) exist = true;
			});
			if ($ctrl.eventsDay.length == 4) $ctrl.disableButton = true;
			return exist;
		};
	}
})();
