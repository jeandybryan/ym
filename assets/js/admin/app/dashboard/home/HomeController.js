(function () {
  "use strict";
  angular.module("YumaMassage").controller("HomeController", HomeController);

  HomeController.$inject = ["$rootScope", "api"];

  function HomeController(rootScope) {
    var vm = this;
    activate();

    ////////////////

    function activate() {
      rootScope.page = "home";
    }
  }
})();
