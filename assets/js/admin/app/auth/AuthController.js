(function() {
	'use strict';

	angular.module('YumaMassage').controller('AuthController', AuthController);

	AuthController.$inject = ['$scope', '$rootScope', 'api', 'jwtHelper', '$localStorage', '$state', '$http'];

	function AuthController(scope, rootScope, api, jwtHelper, localStorage, state, http) {
		var vm = this;
		vm.registerSuccesfull = false;
		vm.doLogin = doLogin;
		vm.doRegister = doRegister;
		vm.showForm = false;
		vm.redirect = redirect;
		vm.login = {
			username: '',
			password: ''
		};
		vm.register = {
			username: '',
			firstname: '',
			lastname: '',
			email: '',
			phone: '',
			gender: 'male',
			password: '',
			r_password: ''
		};
		vm.cardStyle = {
			'margin-top': '100px'
		};

		vm.redirect();

		////////////////

		function activate() {}
		function doLogin() {
			scope.$broadcast('ng-validate');
			if (scope.doLogin.$valid) {
				var btn = $('#btn-in-loading-state');
				btn.button('loading');
				api.signin
					.post({
						username: vm.login.username,
						password: vm.login.password
					})
					.$promise.then(function(response) {
						if (response.token) {
							var tokenPayload = jwtHelper.decodeToken(response.token);
							if (!tokenPayload.user.active) {
								let message = 'Usuario no Activo';
								rootScope.showNotification('danger', message, 'toast-bottom-center');
								btn.button('reset');
							} else {
								// store username and token in local storage to keep user logged in between page refreshes
								localStorage.token = response.token;
								rootScope.currentUser = tokenPayload.user;
								// add jwt token to auth header for all requests made by the $http service
								http.defaults.headers.common.Authorization = 'Bearer ' + response.token;

								// execute callback with true to indicate successful login
								state.go('admin.home');
								btn.button('reset');
							}
						} else {
							// execute callback with false to indicate failed login
							rootScope.showNotification(
								'danger',
								'Usuario o Contraseña incorrectos',
								'toast-bottom-center'
							);
							btn.button('reset');
						}
					})
					.catch(function(response) {
						let message = 'Usuario o Contraseña incorrectos';
						if (response.data.code == 'E_USER_NOT_FOUND')
							message = 'El usuario especificado no se encuentra';
						rootScope.showNotification('danger', message, 'toast-bottom-center');
						btn.button('reset');
					});
			} else {
				if (scope.doLogin.password.$valid && scope.doLogin.username.$valid) {
					vm.recaptchaError = true;
				}
			}
		}

		function doRegister() {
			angular.element('#r_password').removeClass('has-error');
			angular.element('#errorMessagePassword').remove();
			vm.registerSuccesfull = false;
			scope.$broadcast('ng-validate');
			if (vm.register.password !== vm.register.r_password) {
				angular.element('#r_password').addClass('has-error');
				var element = `<span id="errorMessagePassword" ng-show="errorStatus" ng-bind="errorMessage" class="help-block ng-binding ng-scope">Las contraseñas no coinciden</span>`;
				angular.element('#r_password').append(element);
			}
			if (scope.doRegister.$valid && vm.register.password === vm.register.r_password) {
				var btn = $('#btn-up-loading-state');
				btn.button('loading');
				api.signup
					.post({
						username: vm.register.username,
						firstname: vm.register.firstname,
						lastname: vm.register.lastname,
						email: vm.register.email,
						phone: vm.register.phone,
						gender: vm.register.gender,
						password: vm.register.password
					})
					.$promise.then(function(response) {
						if (response.user && response.token) {
							vm.registerSuccesfull = true;
						} else {
							rootScope.showNotification(
								'danger',
								'Ha ocurrido un problema, vuelva ha intentarlo más tarde',
								'toast-bottom-center'
							);
						}
					})
					.catch(function(response) {
						let message = 'Ha ocurrido un problema, vuelva ha intentarlo más tarde';
						let type = 'danger';
						if (response.data.code == 'E_VALIDATION') {
							if (response.data.invalidAttributes.email) {
								angular.forEach(response.data.invalidAttributes.email, function(value) {
									if (value.rule == 'unique') {
										message = 'El EMAIL usado ya está en uso';
										type = 'warning';
									}
								});
							}
							if (response.data.invalidAttributes.username) {
								angular.forEach(response.data.invalidAttributes.username, function(value) {
									if (value.rule == 'unique') {
										message = 'El USUARIO usado ya está en uso';
										type = 'warning';
									}
								});
							}
						}
						rootScope.showNotification(type, message, 'toast-bottom-center');
						btn.button('reset');
					});
			}
		}

		function redirect() {
			if (rootScope.currentUser) {
				state.go('admin.home');
			} else {
				vm.showForm = true;
			}
		}
	}
})();
