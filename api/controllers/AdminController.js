/**
 * AdminController
 *
 * @description :: Server-side logic for managing admins
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  home: function (req, res) {
    res.view('admin/layout', {
      layout: false,
      locals: {
        viewBag: req.viewBag
      }
    })
  }
};

