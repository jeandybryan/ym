/**
 * TherapistController
 *
 * @description :: Server-side logic for managing Therapists
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res) {
		const values = req.allParams();
		TherapistService.find(values)
			.then(results => {
				return res.ok({
					count: results[0],
					users: results[1]
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	findOne: function(req, res) {
		const values = req.allParams();
		TherapistService.findOne(values)
			.then(results => {
				return res.ok({
					users: results
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	create: function(req, res) {
		const values = _.omit(req.allParams(), 'id');

		TherapistService.create(values)
			.then(response => {
				return res.created(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	update: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;

		TherapistService.update(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	profile: function(req, res) {
		const values = req.allParams();
		values.id = req.user.id;

		TherapistService.update(values)
			.then(response => {
				let data = {
					token: CipherService.jwt.encodeSync({
						user: response.user
					})
				};
				return res.ok(data);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	password: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;
		TherapistService.password(values, req.user)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	active: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;
		TherapistService.active(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	destroy: function(req, res) {
		const values = req.allParams();
		values.id = values.slug;
		delete values.slug;
		TherapistService.destroy(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	updateEvents: function(req, res) {
		const values = req.allParams();
		TherapistService.updateEvents(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	}
};
