/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res) {
		const values = req.allParams();
		UserService.find(values)
			.then(results => {
				return res.ok({
					count: results[0],
					users: results[1]
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	findOne: function(req, res) {
		const values = req.allParams();
		UserService.findOne(values)
			.then(results => {
				return res.ok({
					users: results
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	create: function(req, res) {
		const values = _.omit(req.allParams(), 'id');

		UserService.create(values)
			.then(response => {
				return res.created(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	update: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;

		UserService.update(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	profile: function(req, res) {
		const values = req.allParams();
		values.id = req.user.id;
		delete values._id;

		UserService.update(values)
			.then(response => {
				let data = {
					token: CipherService.jwt.encodeSync({
						user: response.user
					})
				};
				return res.ok(data);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	password: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;
		UserService.password(values, req.user)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	profilePassword: function(req, res) {
		const values = req.allParams();
		values.id = req.user.id;
		delete values._id;
		UserService.password(values, req.user)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	active: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;
		UserService.active(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	destroy: function(req, res) {
		const values = req.allParams();
		values.id = values.slug;
		delete values.slug;
		UserService.destroy(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	uploadPhoto: function(req, res) {
		req.file('photos').upload(
			{
				// don't allow the total upload size to exceed ~10MB
				dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/users'),
				maxBytes: 10000000
			},
			function whenDone(err, uploadedFiles) {
				if (err) {
					return res.negotiate(err);
				}
				UserService.uploadPhoto(req.param('id'), uploadedFiles)
					.then(response => {
						return res.ok(response);
					})
					.catch(error => {
						return res.negotiate(error);
					});
			}
		);
	},
	deletePhoto: function(req, res) {
		const values = req.allParams();
		UserService.deletePhoto(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	}
};
