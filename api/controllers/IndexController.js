/**
 * IndexController
 *
 * @description :: Server-side logic for managing indices
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Barrels = require('barrels');
var barrels = new Barrels();

module.exports = {
	install: function(req, res) {
		res.view('admin/install', {
			layout: false
		});
	},
	installApp: function(req, res) {
		async.parallel(
			[
				function(cb) {
					barrels.populate(['user', 'massage'], function(err) {
						if (err) {
							cb(err, null);
						} else {
							cb(null, true);
						}
					});
				}
			],
			function(err, response) {
				if (err) {
					return res.negotiate(err);
				} else {
					return res.ok(response);
				}
			}
		);
	},
	home: function(req, res) {
		MassageService.find({})
			.then(treatments => {
				let array = treatments[1].filter(item => item.prices.length > 0);
				treatments[0] = array.length;
				treatments[1] = array;
				res.view('client/homepage', {
					page: 'homepage',
					layout: 'client/layout',
					locals: {
						viewBag: req.viewBag,
						treatments: treatments || []
					}
				});
			})
			.catch(err => res.negotiate(error));
	},
	therapists: function(req, res) {
		TherapistService.find({})
			.then(therapists => {
				let array = therapists[1].filter(item => item.active);
				therapists[0] = array.length;
				therapists[1] = array;
				res.view('client/therapists', {
					page: 'therapists',
					layout: 'client/layout',
					locals: {
						viewBag: req.viewBag,
						therapists: therapists || []
					}
				});
			})
			.catch(err => res.negotiate(err));
	},
	treatments: function(req, res) {
		MassageService.find({})
			.then(treatments => {
				res.view('client/treatments', {
					page: 'treatments',
					layout: 'client/layout',
					locals: {
						viewBag: req.viewBag,
						treatments: treatments || []
					}
				});
			})
			.catch(err => res.negotiate(error));
	},
	treatment: function(req, res) {
		const values = req.allParams();

		MassageService.findBySlug(values)
			.then(treatment => {
				res.view('client/treatment', {
					page: 'treatments',
					layout: 'client/layout',
					locals: {
						viewBag: req.viewBag,
						treatment: treatment.massage || null
					}
				});
			})
			.catch(err => res.negotiate(err));
	},
	therapist: function(req, res) {
		const values = req.allParams();
		delete values.lang;
		TherapistService.findByUserName(values)
			.then(therapist => {

        therapist.user.media = therapist.medias
				res.view('client/therapist', {
					page: 'therapist',
					layout: 'client/layout',
					locals: {
						daysData: [],
						viewBag: req.viewBag,
						therapist: therapist.user || null,
					}
				});
			})
			.catch(err => res.negotiate(err));
	},
	findOneMedia: function(req, res) {
		Media.findOne(req.param('id'))
			.then(media => {
				if (!media) {
					return res.notFound();
				} else {
					var SkipperDisk = require('skipper-disk');

					var fileAdapter = SkipperDisk(/* optional opts */);
					fileAdapter
						.read(media.dir)
						.on('error', function(err) {
							return res.serverError(err);
						})
						.pipe(res);
				}
			})
			.catch(err => {
				return res.negotiate(error);
			});
	},
	request: function(req, res) {
		const values = req.allParams();
		delete values.lang;
		IndexService.request(values)
			.then(request => {
				res.view('client/request', {
					page: 'request',
					layout: 'client/layout',
					locals: {
						viewBag: req.viewBag,
						therapist: request[1].user || null,
						treatment: request[0].massage || null,
						daysData: request.daysData || [],
						sessions: [
							sails.__({
								phrase: 'seccion_daybreak',
								locale: req.viewBag.lang
							}),
							sails.__({
								phrase: 'seccion_morning',
								locale: req.viewBag.lang
							}),
							sails.__({
								phrase: 'seccion_afternoon',
								locale: req.viewBag.lang
							}),
							sails.__({
								phrase: 'seccion_night',
								locale: req.viewBag.lang
							})
						]
					}
				});
			})
			.catch(err => res.negotiate(err));
	},
	addEvent: function(req, res) {
		const values = req.allParams();
		TherapistService.addEvent(values)
			.then(request => {
				res.ok(request);
			})
			.catch(err => res.negotiate(err));
	},
	run: function(req, res) {
		Media.find()
			.then(medias => {
				async.map(
					medias,
					function(media, cbm) {
						let dir = media.dir.replace('/var/www/vhosts/isofact.net/yumamassage.isofact.net/assets', '');
						Media.update(media.id, { dir: dir })
							.then(data => cbm(data, null))
							.catch(err => cbm(err, null));
					},
					function(err, result) {
						if (err) res.serverError(err);
						res.ok(result);
					}
				);
			})
			.catch(err => res.json(err));
	}
};
