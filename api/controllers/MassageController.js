/**
 * MassageController
 *
 * @description :: Server-side logic for managing Massage
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	find: function(req, res) {
		const values = req.allParams();
		MassageService.find(values)
			.then(results => {
				return res.ok({
					count: results[0],
					massages: results[1]
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	findOne: function(req, res) {
		const values = req.allParams();
		MassageService.findOne(values)
			.then(results => {
				return res.ok({
					massages: results
				});
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	create: function(req, res) {
		const values = _.omit(req.allParams(), 'id');
		delete values.indications;
		delete values.contraindications;

		MassageService.create(values)
			.then(response => {
				return res.created(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	update: function(req, res) {
		const values = req.allParams();
		values.id = values._id;
		delete values._id;

		MassageService.update(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	destroy: function(req, res) {
		const values = req.allParams();
		values.id = values.slug;
		delete values.slug;
		MassageService.destroy(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	},
	uploadPhotos: function(req, res) {
		req.file('photos').upload({
			// don't allow the total upload size to exceed ~10MB
			dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/massages'),
			maxBytes: 10000000
		}, function whenDone(err, uploadedFiles) {
			if (err) {
				return res.negotiate(err);
			}
			MassageService.uploadPhotos(req.param('id'), uploadedFiles)
				.then(response => {
					return res.ok(response);
				})
				.catch(error => {
					return res.negotiate(error);
				});
		});
	},
	deletePhotos: function(req, res) {
		const values = req.allParams();
		MassageService.deletePhotos(values)
			.then(response => {
				return res.ok(response);
			})
			.catch(error => {
				return res.negotiate(error);
			});
	}
};
