module.exports = function(req, res, next) {
	let lang;
	if (!req.viewBag) req.viewBag = {};

	if (!req.session.lang) {
		lang = 'en';
	} else {
		lang = req.session.lang;
	}

	if (req.param('lang')) lang = req.param('lang');

	req.session.languagePreference = lang;
	req.viewBag.lang = lang;
	req.setLocale(req.session.languagePreference);
	next();
};
