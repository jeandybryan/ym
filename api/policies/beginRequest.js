module.exports = function (req, res, next) {
  if (!req.viewBag) req.viewBag = {};
  req.viewBag.user = req.user;
  req.viewBag.date = new Date();
  next();
};
