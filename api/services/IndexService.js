/**
 * IndexService
 *
 * @description :: Server-side logic for managing App
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */
const Joi = require('joi');

function getNameDay(date) {
	if (date == 0) return sails.__('day_Sunday');
	if (date == 1) return sails.__('day_Monday');
	if (date == 2) return sails.__('day_Tuesday');
	if (date == 3) return sails.__('day_Wednesday');
	if (date == 4) return sails.__('day_Thursday');
	if (date == 5) return sails.__('day_Friday');
	if (date == 6) return sails.__('day_Saturday');
}

module.exports = {
	request: function(query) {
		return new Promise(function(resolve, reject) {
			let day;
			if (query.day) {
				day = new Date(query.day);
			} else {
				day = new Date();
			}

			let mquery = {
				slug: query.slug
			};
			let tquery = {
				username: query.username
			};

			Promise.all([MassageService.findBySlug(mquery), TherapistService.findByUserName(tquery)])
				.then(results => {
					let days = [];
					let min = new Date(day.getFullYear(), day.getMonth() - 1, day.getDate());
					let startAt = 0,
						aux = 0,
						count = 0;
					let max = new Date(day.getFullYear(), day.getMonth() + 1, day.getDate());
					while (min < max) {
						if (
							day.getFullYear() == min.getFullYear() &&
							day.getMonth() == min.getMonth() &&
							day.getDate() == min.getDate()
						) {
							startAt = count;
							aux = count;
						}

						let events = [];
						if (results[1].user.therapist.events && results[1].user.therapist.events.length) {
							events = results[1].user.therapist.events.filter(
								event =>
									new Date(event.start).getMonth() === min.getMonth() &&
									new Date(event.start).getFullYear() === min.getFullYear() &&
									new Date(event.start).getDate() == min.getDate()
							);
						}

						days.push({
							events: events,
							d: min.getDate(),
							nameD: getNameDay(min.getDay()),
							m: min.getMonth(),
							y: min.getFullYear(),
							active: aux != 0 ? true : false
						});
						min.setDate(min.getDate() + 1);
						aux = 0;
						count++;
					}
					results.daysData = {
						days: days,
						startAt: startAt
					};
					resolve(results);
				})
				.catch(error => reject(error));
		});
	}
};
