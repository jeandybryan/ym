/**
 * MassageService
 *
 * @description :: Server-side logic for managing Massages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */
const Joi = require('joi');

module.exports = {
	find: function(query) {
		return new Promise(function(resolve, reject) {
			query.removedAt = null;
			query.limit = query.limit || 10;
			query.skip = query.skip || 0;

			var massages = query.massages ? JSON.parse(query.massages) : [];
			if (massages.length) {
				var array = [];
				massages.forEach(function(element) {
					array.push(element.id);
				}, this);
				query.id = { '!': array };
			}
			delete query.massages;

			Promise.all([Massage.count(query), Massage.find(query).populateAll()])
				.then(results => {
					async.map(
						results[1],
						function(elem, cbm) {
							cbm(null, elem.toJSON());
						},
						function(err, res) {
							if (err) reject(err);
							else {
								results[1] = res;
								resolve(results);
							}
						}
					);
				})
				.catch(error => reject(error));
		});
	},
	findOne: function(query) {
		query.removedAt = null;
		return Massage.findOne(query);
	},
	findBySlug: function(query) {
		return new Promise(function(resolve, reject) {
			query.removedAt = null;
			Massage.findOne({ slug: query.slug })
				.populateAll()
				.then(massage => {
					massage.prices = massage.prices.sort(function(a, b) {
						return a.price - a.price;
					});
					if (massage.therapists.length) {
						async.map(
							massage.therapists,
							function(therapist, cbm) {
								User.findOne(therapist.user)
									.then(th => {
										th
											.therapistFormat(th.id)
											.then(ther => {
												cbm(null, ther);
											})
											.catch(err => {
												cbm(err, null);
											});
									})
									.catch(err => {
										cbm(err, null);
									});
							},
							function(err, results) {
								if (err) return reject(err);
								massage = massage.toJSON();
								massage.therapists = results;
								return resolve({
									massage: massage
								});
							}
						);
					} else {
						resolve({
							massage: massage.toJSON()
						});
					}
				})
				.catch(error => reject(error));
		});
	},
	create: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				color: Joi.string().required(),
				name: Joi.object()
					.keys({
						es: Joi.string().required(),
						en: Joi.string().required()
					})
					.required(),
				description: Joi.object()
					.keys({
						es: Joi.string().required(),
						en: Joi.string().required()
					})
					.required()
			});
			const result = Joi.validate(query, schema);
			if (result.error === null) {
				Massage.create(query)
					.then(massage => {
						resolve({
							massage: massage
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	update: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string().required(),
				color: Joi.string().required(),
				name: Joi.object()
					.keys({
						es: Joi.string().required(),
						en: Joi.string().required()
					})
					.required(),
				description: Joi.object()
					.keys({
						es: Joi.string().required(),
						en: Joi.string().required()
					})
					.required(),
				indications: Joi.array(),
				contraindications: Joi.array(),
				prices: Joi.array()
			});
			const result = Joi.validate(query, schema);

			if (result.error === null) {
				MassageService.updatePrice(query)
					.then(prices => {
						var array2 = [];
						if (prices.length) {
							prices.forEach(function(element) {
								array2.push(element.id);
							}, this);
							query.prices = array2;
						}
						Massage.update({ id: query.id }, query)
							.then(massages => {
								let massage = massages[0] || null;
								if (massages[0]) {
									Massage.findOne(massages[0].id)
										.populateAll()
										.then(massage => {
											resolve({
												massage: massage
											});
										})
										.catch(error => reject(error));
								} else {
									resolve({
										massage: null
									});
								}
							})
							.catch(error => reject(error));
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	destroy: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				Massage.update(
					{ id: query.id },
					{
						removedAt: new Date()
					}
				)
					.then(massages => {
						resolve({
							massage: massages[0] || null
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	uploadPhotos: function(id, files) {
		return new Promise(function(resolve, reject) {
			Massage.findOne(id)
				.then(massage => {
					// massage = massage.toJSON();
					if (massage && massage.medias.length < 10) {
						// If no files were uploaded, respond with an error.
						if (files.length === 0) {
							reject(sails.config.errors.BAD_REQUEST);
						}
						let array = [];
						let path = process.cwd();

						files.forEach(function(file) {

							let dir = file.fd.replace(path + '/assets', '');
							let aux = {
								dir: dir,
								filename: file.filename,
								size: file.size,
								massage: massage.id
							};
							array.push(aux);
						});

						Media.create(array)
							.then(medias => {
								Massage.findOne(id)
									.then(massage => {
										Massage.findOne(massage.id)
											.populateAll()
											.then(mass => {
												resolve({
													massage: mass.toJSON()
												});
											})
											.catch(err => reject(err));
									})
									.catch(err => reject(err));
							})
							.catch(err => reject(err));
					} else {
						reject(sails.config.errors.PHOTOS_IS_FULL);
					}
				})
				.catch(err => {
					reject(err);
				});
		});
	},
	deletePhotos: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				Media.destroy(query.id)
					.then(response => {
						resolve({
							response: response[0] || null
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	updatePrice: function(query) {
		return new Promise(function(resolve, reject) {
			Massage.findOne(query.id)
				.populate('prices')
				.then(massage => {
					if (massage.prices && massage.prices.length) {
						if (query.prices.length) {
							let ids1 = massage.prices.map(item => item.id);
							let ids2 = query.prices.map(item => item.id);

							let diff = ids1
								.map((id, index) => {
									if (ids2.indexOf(id) < 0) {
										return massage.prices[index];
									}
								})
								.concat(
									ids2.map((id, index) => {
										if (ids1.indexOf(id) < 0) {
											return query.prices[index];
										}
									})
								)
								.filter(item => item != undefined);

							async.map(
								diff,
								function(elem, cbm) {
									if (elem.id == 'new') {
										Price.create(elem)
											.then(price => {
												cbm(null, price);
											})
											.catch(err => {
												cbm(err, null);
											});
									} else {
										Price.destroy(elem.id)
											.then(price => {
												price.rm = true;
												cbm(null, price);
											})
											.catch(err => {
												cbm(err, null);
											});
									}
								},
								function(err, result) {
									if (err) return reject(error);
									else {
										var array = [];
										query.prices.forEach(function(elem) {
											if (elem.id != 'new') array.push(elem);
										});
										result.forEach(function(elem) {
											if (!elem.rm) array.push(elem);
										});
										resolve(array);
									}
								}
							);
						} else {
							async.map(
								massage.prices,
								function(price, cbm) {
									Price.destroy(price.id)
										.then(price => {
											cbm(null, price);
										})
										.catch(error => cbm(error, null));
								},
								function(err, result) {
									if (err) return reject(error);
									else {
										resolve([]);
									}
								}
							);
						}
					} else {
						Price.create(query.prices)
							.then(prices => {
								resolve(prices);
							})
							.catch(error => reject(error));
					}
				})
				.catch(error => reject(error));
		});
	}
};
