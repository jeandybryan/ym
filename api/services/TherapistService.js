/**
 * TherapistService
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */
const Joi = require('joi');
let moment = require('moment');

function matchCalendarBlock(a, b, c, d) {
  if (b.getFullYear() <= 1970) return true;
  if (c < a && a < d) {
    return true;
  }
  if (c < b && b < d) {
    return true;
  }
  if (a < c && c < b) {
    return true;
  }
  if (a < d && d < b) {
    return true;
  }
  if (a.getTime() === c.getTime() && b.getTime() === d.getTime()) {
    return true;
  }
  return false;
}

module.exports = {
  find: function (query) {
    return new Promise(function (resolve, reject) {
      query.removedAt = null;
      query.isTherapist = true;
      query.limit = query.limit;
      query.skip = query.skip || 0;

      Promise.all([User.count(query), User.find(query)])
        .then(results => {
          async.map(
            results[1],
            function (user, cbm) {
              user
                .therapistFormat(user.id)
                .then(therapist => {
                  cbm(null, therapist);
                })
                .catch(err => {
                  cbm(err, null);
                });
            },
            function (err, response) {
              results[1] = response;
              resolve(results);
            }
          );
        })
        .catch(error => reject(error));
    });
  },
  findOne: function (query) {
    query.removedAt = null;
    query.isTherapist = true;
    return User.findOne(query);
  },
  findByUserName: function (query) {
    query.removedAt = null;
    query.isTherapist = true;
    let today = new Date();
    let date = query.date || {m: today.getMonth(), y: today.getFullYear()};
    delete query.date;
    delete query;
    return new Promise(function (resolve, reject) {
      User.findOne(query)
        .then(user => {
          user
            .therapistFormat(user.id)
            .then(therapist => {
              let contD = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
              let nd = new Date(today.getFullYear(), today.getMonth(), 1).getDay();
              let days = [];
              for (let i = 0; i < nd; i++) {
                days.push(new Date(new Date(today.getFullYear(), today.getMonth(), -i)));
              }

              let max = 42 - nd - contD;
              for (let i = 0; i < contD + max; i++) {
                days.push(new Date(new Date(today.getFullYear(), today.getMonth(), 1 + i)));
              }
              let FDays = [];
              let fEvents = [];
              for (let i = 0; i < days.length; i++) {
                if (therapist.therapist.events && therapist.therapist.events.length) {
                  fEvents = therapist.therapist.events.filter(
                    event =>
                      new Date(event.start).getMonth() === days[i].getMonth() &&
                      new Date(event.start).getFullYear() === days[i].getFullYear() &&
                      new Date(event.start).getDate() === days[i].getDate()
                  );
                }
                let aux = {
                  num: i,
                  day: days[i].getDate(),
                  events: fEvents,
                  lock:
                    fEvents.length === 4
                      ? true
                      : fEvents.length === 1 && fEvents[0].title === 'Día bloqueado',
                  thisM: days[i].getMonth() === today.getMonth()
                };
                FDays.push(aux);
              }

              therapist.calendar = FDays;
              resolve({
                user: therapist
              });
            })
            .catch(err => reject(err));
        })
        .catch(err => reject(err));
    });
  },
  create: function (query) {
    delete query.therapist.medias;
    return new Promise(function (resolve, reject) {
      const schema = Joi.object().keys({
        username: Joi.string().required(),
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        email: Joi.string().email(),
        phone: Joi.string().required(),
        gender: Joi.string()
          .allow('male', 'female')
          .required(),
        password: Joi.string()
          .regex(/^[a-zA-Z0-9]{3,30}$/)
          .required(),
        therapist: Joi.object()
          .keys({
            yxp: Joi.number().required(),
            languages: Joi.array(),
            treatments: Joi.array(),
            week_day: Joi.boolean(),
            weekend: Joi.boolean(),
            morning: Joi.boolean(),
            afternoon: Joi.boolean(),
            evening: Joi.boolean(),
            daybreak: Joi.boolean(),
            description: Joi.object()
              .keys({
                es: Joi.string().required(),
                en: Joi.string().required()
              })
              .required()
          })
          .required()
      });

      const result = Joi.validate(query, schema);

      if (result.error === null) {
        query.isTherapist = true;
        let array = [];
        if (query.therapist.treatments.length) {
          query.therapist.treatments.forEach(function (element) {
            array.push(element.id);
          }, this);
          query.therapist.treatments = array;
        }
        User.create(query)
          .then(user => {
            user
              .therapistFormat(user.id)
              .then(therapist => {
                resolve({
                  user: therapist
                });
              })
              .catch(err => reject(err));
          })
          .catch(error => reject(error));
      } else {
        reject(sails.config.errors.BAD_REQUEST);
      }
    });
  },
  update: function (query) {
    return new Promise(function (resolve, reject) {
      if (query.therapist && query.therapist.events) delete query.therapist.events;

      const schema = Joi.object().keys({
        id: Joi.string()
          .alphanum()
          .required(),
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        email: Joi.string().email(),
        phone: Joi.string().required(),
        gender: Joi.string()
          .allow('male', 'female')
          .required(),
        therapist: Joi.object().keys({
          id: Joi.string()
            .alphanum()
            .required(),
          yxp: Joi.number().required(),
          languages: Joi.array(),
          treatments: Joi.array(),
          week_day: Joi.boolean(),
          weekend: Joi.boolean(),
          morning: Joi.boolean(),
          afternoon: Joi.boolean(),
          evening: Joi.boolean(),
          daybreak: Joi.boolean(),
          medias: Joi.array(),
          description: Joi.object().keys({
            es: Joi.string().required(),
            en: Joi.string().required()
          })
        })
      });
      if (query.therapist) {
        delete query.therapist.user;
        delete query.therapist.createdAt;
        delete query.therapist.updatedAt;
      }

      const result = Joi.validate(query, schema);

      if (result.error === null) {
        User.update({id: query.id}, query)
          .then(users => {
            users[0]
              .therapistFormat(users[0].id)
              .then(therapist => {
                resolve({
                  user: therapist
                });
              })
              .catch(err => reject(err));
          })
          .catch(error => reject(error));
      } else {
        reject(sails.config.errors.BAD_REQUEST);
      }
    });
  },
  password: function (query, currentUser) {
    return new Promise(function (resolve, reject) {
      let result;
      if (currentUser.isAdmin) {
        const schema = Joi.object().keys({
          id: Joi.string()
            .alphanum()
            .required(),
          password: Joi.string()
            .regex(/^[a-zA-Z0-9]{3,30}$/)
            .required()
        });
        delete query.old_password;
        result = Joi.validate(query, schema);
      } else {
        const schema = Joi.object().keys({
          id: Joi.string()
            .alphanum()
            .required(),
          password: Joi.string()
            .regex(/^[a-zA-Z0-9]{3,30}$/)
            .required(),
          old_password: Joi.string()
            .regex(/^[a-zA-Z0-9]{3,30}$/)
            .required()
        });
        result = Joi.validate(query, schema);
      }

      if (result.error === null) {
        if (currentUser.isAdmin) {
          User.update(
            {id: query.id},
            {
              password: query.password
            }
          )
            .then(users => {
              resolve({
                user: users[0] || null
              });
            })
            .catch(error => reject(error));
        } else {
          User.findOne({id: query.id})
            .then(user => {
              if (!HashService.bcrypt.compareSync(query.password, user.password)) {
                reject(sails.config.errors.USER_NOT_FOUND);
              } else {
                User.update(
                  {id: query.id},
                  {
                    password: query.password
                  }
                )
                  .then(users => {
                    resolve({
                      user: users[0] || null
                    });
                  })
                  .catch(error => reject(error));
              }
            })
            .catch(error => reject(error));
        }
      } else {
        reject(sails.config.errors.BAD_REQUEST);
      }
    });
  },
  active: function (query) {
    return new Promise(function (resolve, reject) {
      const schema = Joi.object().keys({
        id: Joi.string()
          .alphanum()
          .required(),
        active: Joi.boolean().required()
      });

      const result = Joi.validate(query, schema);

      if (result.error === null) {
        User.update(
          {id: query.id},
          {
            active: query.active
          }
        )
          .then(users => {
            resolve({
              user: users[0] || null
            });
          })
          .catch(error => reject(error));
      } else {
        reject(sails.config.errors.BAD_REQUEST);
      }
    });
  },
  destroy: function (query) {
    return new Promise(function (resolve, reject) {
      const schema = Joi.object().keys({
        id: Joi.string()
          .alphanum()
          .required()
      });

      const result = Joi.validate(query, schema);

      if (result.error === null) {
        User.update(
          {id: query.id},
          {
            removedAt: new Date()
          }
        )
          .then(users => {
            resolve({
              user: users[0] || null
            });
          })
          .catch(error => reject(error));
      } else {
        reject(sails.config.errors.BAD_REQUEST);
      }
    });
  },
  updateEvents: function (query) {
    return new Promise(function (resolve, reject) {
      Therapist.findOne(query.id)
        .then(therapist => {
          let events = JSON.parse(query.events);
          let fevents = [];
          if (events.length) {
            _.forEach(events, function (elem) {
              elem.start = moment({
                year: elem.start.y,
                month: elem.start.m,
                day: elem.start.d,
                hour: elem.start.h,
                minute: elem.start.mi,
                second: 0,
                millisecond: 0
              }).valueOf();
              if (elem.end) {
                elem.end = moment({
                  year: elem.end.y,
                  month: elem.end.m,
                  day: elem.end.d,
                  hour: elem.end.h,
                  minute: elem.end.mi,
                  second: 0,
                  millisecond: 0
                }).valueOf();
              }
            });

            let minDay = moment({
              year: query.day.y,
              month: query.day.m,
              day: query.day.d
            });

            let maxDay = moment({
              year: query.day.y,
              month: query.day.m,
              day: parseInt(query.day.d) + 1
            });

            if (therapist.events && therapist.events.length) {
              fevents = therapist.events.filter(
                event =>
                  !matchCalendarBlock(
                    new Date(event.start),
                    new Date(event.end),
                    new Date(minDay.valueOf()),
                    new Date(maxDay.valueOf())
                  )
              );
            }
            fevents = fevents.concat(events);
          }

          Therapist.update(
            {id: query.id},
            {
              events: fevents
            }
          )
            .then(therapists => {
              resolve({
                therapist: therapists[0] || null
              });
            })
            .catch(error => {
              reject(error);
            });
        })
        .catch(error => reject(error));
    });
  },

  addEvent: function (query) {
    return new Promise(function (resolve, reject) {

      Promise.all([Therapist.findOne(query.therapist), Price.findOne(query.price), Massage.findOne(query.treatment)])
        .then(results => {
          let therapist = results[0]
          let price = results[1]
          let treatment = results[2]
          let events = [];
          let date = moment.unix(query.date),
            time = query.time,
            h = parseInt(time.split(":")[0]),
            m = time.split(":")[1].split(" ")[0],
            mt = time.split(":")[1].split(" ")[1];
          if (mt === "pm") {
            h = h + 12
          }

          let minDay = moment({
            year: date.year(),
            month: date.month(),
            day: date.date(),
            hour: h,
            minute: m,
            second: 0,
            millisecond: 0
          });
          let maxDay = moment({
            year: date.year(),
            month: date.month(),
            day: date.date(),
            hour: h,
            minute: m,
            second: 0,
            millisecond: 0
          });
          maxDay.add(price.time, 'minutes');

          if (therapist.events && therapist.events.length) {
            events = therapist.events;
            let tevents = therapist.events.filter(
              event =>
                !matchCalendarBlock(
                  new Date(event.start),
                  new Date(event.end),
                  new Date(minDay.valueOf()),
                  new Date(maxDay.valueOf())
                )
            );
            events = events.concat(tevents);
          }
          else {
            let event = {
              type: "book",
              title: "Reservación",
              start: minDay.valueOf(),
              end: maxDay.valueOf(),
              allDay: false,
              color: treatment.color,
              textColor: "white"
            }
            events.push(event);
          }

          Therapist.update(
            {id: therapist.id},
            {
              events: events
            }
          )
            .then(therapists => {
              resolve({
                therapist: therapists[0] || null
              });
            })
            .catch(error => {
              reject(error);
            });


        })
        .catch(error => reject(error));
    });
  }
};
