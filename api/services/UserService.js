/**
 * UserService
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Services
 */
const Joi = require('joi');

module.exports = {
	find: function(query) {
		return new Promise(function(resolve, reject) {
			query.removedAt = null;
			query.limit = query.limit;
			query.skip = query.skip || 0;
			query.isTherapist = false;
			Promise.all([User.count(query), User.find(query)])
				.then(results => {
					resolve(results);
				})
				.catch(error => reject(error));
		});
	},
	findOne: function(query) {
		query.removedAt = null;
		query.isTherapist = false;
		return User.findOne(query);
	},
	signup: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				username: Joi.string()
					.alphanum()
					.required(),
				firstname: Joi.string()
					.alphanum()
					.required(),
				lastname: Joi.string()
					.alphanum()
					.required(),
				email: Joi.string().email(),
				phone: Joi.string().required(),
				gender: Joi.string()
					.allow('male', 'female')
					.required(),
				password: Joi.string()
					.regex(/^[a-zA-Z0-9]{3,30}$/)
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				User.create(query)
					.then(user => {
						resolve({
							token: CipherService.jwt.encodeSync({
								id: user.id
							}),
							user: user
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	create: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				username: Joi.string()
					.alphanum()
					.required(),
				firstname: Joi.string()
					.alphanum()
					.required(),
				lastname: Joi.string()
					.alphanum()
					.required(),
				email: Joi.string().email(),
				phone: Joi.string().required(),
				gender: Joi.string()
					.allow('male', 'female')
					.required(),
				password: Joi.string()
					.regex(/^[a-zA-Z0-9]{3,30}$/)
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				User.create(query)
					.then(user => {
						resolve({
							user: user
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	update: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required(),
				firstname: Joi.string().required(),
				lastname: Joi.string().required(),
				email: Joi.string().email(),
				phone: Joi.string().required(),
				gender: Joi.string()
					.allow('male', 'female')
					.required()
			});
			const result = Joi.validate(query, schema);

			if (result.error === null) {
				User.update({ id: query.id }, query)
					.then(users => {
						if (users[0].isTherapist) {
							users[0]
								.therapistFormat(users[0].id)
								.then(response => {
									resolve({
										user: response || null
									});
								})
								.catch(err => {
									return res.negotiate(err);
								});
						} else {
							let user = users[0].toJSON();
							resolve({
								user: user || null
							});
						}
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	password: function(query, currentUser) {
		return new Promise(function(resolve, reject) {
			let result;
			if (currentUser.isAdmin) {
				const schema = Joi.object().keys({
					id: Joi.string()
						.alphanum()
						.required(),
					password: Joi.string()
						.regex(/^[a-zA-Z0-9]{3,30}$/)
						.required()
				});
				delete query.old_password;
				result = Joi.validate(query, schema);
			} else {
				const schema = Joi.object().keys({
					id: Joi.string()
						.alphanum()
						.required(),
					password: Joi.string()
						.regex(/^[a-zA-Z0-9]{3,30}$/)
						.required(),
					old_password: Joi.string()
						.regex(/^[a-zA-Z0-9]{3,30}$/)
						.required()
				});
				result = Joi.validate(query, schema);
			}

			if (result.error === null) {
				if (currentUser.isAdmin) {
					User.update(
						{ id: query.id },
						{
							password: query.password
						}
					)
						.then(users => {
							resolve({
								user: users[0] || null
							});
						})
						.catch(error => reject(error));
				} else {
					User.findOne({ id: query.id })
						.then(user => {
							if (!HashService.bcrypt.compareSync(query.old_password, user.password)) {
								reject(sails.config.errors.USER_NOT_FOUND);
							} else {
								User.update(
									{ id: query.id },
									{
										password: query.password
									}
								)
									.then(users => {
										resolve({
											user: users[0] || null
										});
									})
									.catch(error => reject(error));
							}
						})
						.catch(error => reject(error));
				}
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	active: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required(),
				active: Joi.boolean().required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				User.update(
					{ id: query.id },
					{
						active: query.active
					}
				)
					.then(users => {
						resolve({
							user: users[0] || null
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	destroy: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				User.update(
					{ id: query.id },
					{
						removedAt: new Date()
					}
				)
					.then(users => {
						resolve({
							user: users[0] || null
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	},
	uploadPhoto: function(id, files) {
		return new Promise(function(resolve, reject) {
			User.findOne(id)
				.populate('medias')
				.then(user => {
					// user = user.toJSON();
					if (user) {
						// If no files were uploaded, respond with an error.
						if (files.length === 0) {
							reject(sails.config.errors.BAD_REQUEST);
						}
						let array = [];
						let path = process.cwd();
						files.forEach(function(file) {
							let dir = file.fd.replace(path + '/assets', '');
							let aux = {
								dir: dir,
								filename: file.filename,
								size: file.size,
								user: user.id
							};
							array.push(aux);
						});
						if (user.medias.length < 1) {
							Media.create(array)
								.then(medias => {
									User.findOne(id)
										.populateAll()
										.then(user => {
											resolve({
												user: user.toJSON()
											});
										})
										.catch(err => reject(err));
								})
								.catch(err => reject(err));
						} else {
							Media.update(user.medias[0].id, array[0])
								.then(medias => {
									User.findOne(id)
										.populateAll()
										.then(user => {
											resolve({
												user: user.toJSON()
											});
										})
										.catch(err => reject(err));
								})
								.catch(err => reject(err));
						}
					} else {
						reject(sails.config.errors.PHOTOS_IS_FULL);
					}
				})
				.catch(err => {
					reject(err);
				});
		});
	},
	deletePhoto: function(query) {
		return new Promise(function(resolve, reject) {
			const schema = Joi.object().keys({
				id: Joi.string()
					.alphanum()
					.required()
			});

			const result = Joi.validate(query, schema);

			if (result.error === null) {
				Media.destroy(query.id)
					.then(response => {
						resolve({
							response: response[0] || null
						});
					})
					.catch(error => reject(error));
			} else {
				reject(sails.config.errors.BAD_REQUEST);
			}
		});
	}
};
