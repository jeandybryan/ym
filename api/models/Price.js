/**
 * Price.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	attributes: {
		price: { type: 'float', required: true, defaultsTo: 0 }, //USD
		time: { type: 'integer', required: true, defaultsTo: 0 }, //MIN
		massage: { collection: 'massage', via: 'prices' },
		toJSON: function() {
			var obj = this.toObject();
			return obj;
		}
	}
};
