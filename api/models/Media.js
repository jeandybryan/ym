/**
 * Media.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	attributes: {
		dir: { type: 'string', required: true },
		filename: { type: 'string', required: true },
		size: { type: 'integer', required: true },
		// Reference to Massage
		massage: {
			model: 'massage'
		},
		user: {
			model: 'user',
			unique: true
		},
		toJSON() {
			let obj = this.toObject();
			delete obj.password;
			return obj;
		}
	}
};
