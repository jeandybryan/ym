/**
 * Therapist.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var Joi = require('joi');

module.exports = {
  attributes: {
    user: {
      model: 'user',
      unique: true
    },
    description: {
      type: 'json',
      langFormat: true,
      required: true
    },
    yxp: {type: 'integer', required: true}, //A
    treatments: {
      collection: 'massage',
      via: 'therapists'
    },
    languages: {type: 'array'},
    week_day: {type: 'boolean'},
    weekend: {type: 'boolean'},
    morning: {type: 'boolean'},
    afternoon: {type: 'boolean'},
    evening: {type: 'boolean'},
    daybreak: {type: 'boolean'},
    events: {
      type: 'json',
      eventFormat: true
    },
    toJSON() {
      let obj = this.toObject();
      return obj;
    }
  },
  therapistFormat: function (value) {
    return new Promise(function (resolve, reject) {
      Therapist.find(value.id)
        .populateAll()
        .then(therapist => {
          let user = therapist.user;
          delete therapist.user;
          user.therapist = therapist;
          resolve({
            user: user
          });
        })
        .catch(err => reject(err));
    });
  },
  types: {
    langFormat: function (value) {
      var schema = Joi.object().keys({
        es: Joi.string().required(),
        en: Joi.string().required()
      });
      var result = Joi.validate(value, schema);
      if (result.error) {
        return false;
      } else {
        return true;
      }
    },
    eventFormat: function (value) {
      var schema = Joi.array().items(
        Joi.object().keys({
          title: Joi.string().required(),
          start: Joi.number()
            .integer()
            .required(),
          end: Joi.number().allow(null),
          type: Joi.string().required(),
          allDay: Joi.boolean(),
          color: Joi.string(),
          textColor: Joi.string()
        })
      );
      var result = Joi.validate(value, schema);
      if (result.error) {
        return false;
      } else {
        return true;
      }
    }
  }
};
