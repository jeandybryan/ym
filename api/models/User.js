/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
let bcrypt = require('bcrypt-nodejs');
var Joi = require('joi');
var nestedPop = require('nested-pop');

module.exports = {
	attributes: {
		username: { type: 'string', required: true, unique: true },
		firstname: { type: 'string', required: true },
		lastname: { type: 'string', required: true },
		email: { type: 'string', email: true, required: true, unique: true },
		password: { type: 'string', required: true },
		phone: { type: 'string', required: true },
		gender: { type: 'string', enum: ['male', 'female'], defaultsTo: 'male' },
		active: { type: 'boolean', required: true, defaultsTo: true },
		isAdmin: { type: 'boolean', required: true, defaultsTo: false },
		isTherapist: { type: 'boolean', required: true, defaultsTo: false },
		therapist: {
			collection: 'therapist',
			via: 'user'
		},
		medias: {
			collection: 'media',
			via: 'user'
		},
		removedAt: {
			type: 'date',
			defaultsTo: null
		},
		toJSON() {
			let obj = this.toObject();
			delete obj.password;
			return obj;
		},
		therapistFormat: function(userid) {
			return new Promise(function(resolve, reject) {
				User.findOne(userid)
					.populateAll()
					.then(user => {
						Therapist.findOne({ user: user.id })
							.populateAll()
							.then(therapist => {
								delete user.therapist;
								async.map(
									therapist.treatments,
									function(treatment, cbm) {
										Massage.findOne(treatment.id)
											.populate('medias')
											.then(tr => {
												cbm(null, tr);
											})
											.catch(err => {
												cbm(err, null);
											});
									},
									function(err, result) {
										if (err) return reject(err);
										therapist.treatments = result;
										user = JSON.parse(JSON.stringify(user));
										user.therapist = therapist;
										resolve(user);
									}
								);
							})
							.catch(err => reject(err));
					})
					.catch(err => reject(err));
			});
		}
	},
	beforeUpdate(values, next) {
		if (false === values.hasOwnProperty('password')) return next();
		if (/^\$2[aby]\$[0-9]{2}\$.{53}$/.test(values.password)) return next();

		return HashService.bcrypt
			.hash(values.password)
			.then(hash => {
				values.password = hash;
				next();
			})
			.catch(next);
	},
	beforeCreate(values, next) {
		if (false === values.hasOwnProperty('password')) return next();
		return HashService.bcrypt
			.hash(values.password)
			.then(hash => {
				values.password = hash;
				next();
			})
			.catch(next);
	}
};
