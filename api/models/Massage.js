/**
 * Massage.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var Joi = require('joi');
var slug = require('slug');

module.exports = {
	attributes: {
		name: {
			type: 'json',
			langFormat: true,
			required: true
		},
		description: {
			type: 'json',
			langFormat: true,
			required: true
		},
		medias: {
			collection: 'media',
			via: 'massage'
		},
		indications: {
			type: 'json',
			arrayFormat: true,
		},
		contraindications: {
			type: 'json',
			arrayFormat: true,
		},
		therapists: {
			collection: 'therapist',
			via: 'treatments'
		},
		prices: {
			collection: 'price',
			via: 'massage',
			dominant: true
		},
		color: { type: 'string', required: true, defaultsTo: '#000' },
		slug: { type: 'string' },
		toJSON: function() {
			var obj = this.toObject();
			delete obj.imagesFd;
			return obj;
		}
	},
	beforeUpdate(values, next) {
		if (values.name && values.name.es) values.slug = slug(values.name.es).toLowerCase();
		return next();
	},
	beforeCreate(values, next) {
		if (values.name && values.name.es) values.slug = slug(values.name.es).toLowerCase();
		return next();
	},
	types: {
		arrayFormat: function(value) {
			var schema = Joi.array().items(
				Joi.object().keys({
					es: Joi.string().required(),
					en: Joi.string().required()
				})
			);
			var result = Joi.validate(value, schema);
			if (result.error) {
				return false;
			} else {
				return true;
			}
		}
	}
};
